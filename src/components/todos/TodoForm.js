import React, { useState, useEffect, useRef } from "react";

function TodoForm(props) {
  const [input, setInput] = useState(props.edit ? props.edit.value : "");

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  const handleChange = (e) => {
    setInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    props.onSubmit({
      id: Math.floor(Math.random() * 10000),
      text: input,
    });
    setInput("");
  };

  return (
    <div className="flex justify-center">
      <form onSubmit={handleSubmit} className="todo-form">
        {props.edit ? (
          <div className="flex justify-center item-center">
            <div className="flex rounded-xl">
              <div>
                <input
                  placeholder="Update your item"
                  value={input}
                  onChange={handleChange}
                  name="text"
                  ref={inputRef}
                  className="todo-input edit"
                />
                <button
                  onClick={handleSubmit}
                  className="font-serif text-lg text-yellow-100 h-14 bg-cyan-900 rounded-xl md:text-2xl todo-button edit"
                >
                  Update
                </button>
              </div>
            </div>
          </div>
        ) : (
          <div className="flex items-center justify-center">
            <div className="flex rounded-xl">
              <div>
                <input
                  placeholder="Add a todo"
                  value={input}
                  onChange={handleChange}
                  name="text"
                  className="todo-input"
                  ref={inputRef}
                />
              </div>
              <div className="flex items-center justify-center">
                <div>
                  <button
                    onClick={handleSubmit}
                    className="font-serif text-sm text-yellow-100 h-14 bg-cyan-900 rounded-xl md:text-2xl todo-button"
                  >
                    Add todo
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
      </form>
    </div>
  );
}

export default TodoForm;
