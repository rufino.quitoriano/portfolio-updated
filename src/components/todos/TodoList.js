import React, { useState } from "react";
import Slide from "react-reveal/Slide";
// import "./App.css";
import { Link } from "react-router-dom";
import TodoForm from "./TodoForm";
import Todo from "./Todo";

function TodoList() {
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    if (!todo.text || /^\s*$/.test(todo.text)) {
      return;
    }

    const newTodos = [todo, ...todos];

    setTodos(newTodos);
    console.log(...todos);
  };

  const updateTodo = (todoId, newValue) => {
    if (!newValue.text || /^\s*$/.test(newValue.text)) {
      return;
    }

    setTodos((prev) =>
      prev.map((item) => (item.id === todoId ? newValue : item))
    );
  };

  const removeTodo = (id) => {
    const removedArr = [...todos].filter((todo) => todo.id !== id);

    setTodos(removedArr);
  };

  const completeTodo = (id) => {
    let updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };

  return (
    <div className="min-h-screen bg-cyan-900">
      <div className="h-40"></div>
      <div className="flex items-center justify-center">
        <div className="w-full py-12 bg-teal-500 md:w-2/5 rounded-xl">
          <Slide top>
            <div className="flex justify-center my-4 text-2xl font-bold tracking-widest text-center text-yellow-100 md:text-7xl">
              My Todo List
            </div>
          </Slide>
          <div className="flex justify-center">
            <TodoForm onSubmit={addTodo} />
          </div>
          <Todo
            todos={todos}
            completeTodo={completeTodo}
            removeTodo={removeTodo}
            updateTodo={updateTodo}
          />
        </div>
      </div>
      <div className="flex justify-center pt-4 text-2xl text-yellow-100 ">
        <Link to="/">
          <button className="border-2 ">Back</button>
        </Link>
      </div>
    </div>
  );
}

export default TodoList;
