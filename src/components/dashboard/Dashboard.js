import React from "react";
import Body from "./components/Body";
import Navbar from "./components/Navbar";

function Dashboard() {
  return (
    <div className="absolute">
      <Navbar />
      <Body />
    </div>
  );
}

export default Dashboard;
