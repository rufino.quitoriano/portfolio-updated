import React from "react";

function Csr() {
  return (
    <div className="mt-8 px-52">
      <div className="flex items-center justify-between h-32 text-4xl">
        <div>
          <div>Customer Service Representative</div>
          <div className="flex">
            <div className="mr-2">
              <i class="fa fa-map-marker" />
            </div>
            <div>Makati Metro Manila, PH</div>
          </div>
        </div>
        <div className="flex">
          <div className="flex justify-center w-full h-12 m-4 text-center border-2 rounded-full">
            <div className="w-4 h-4 mt-3 mr-2 bg-green-400 border-2 rounded-full" />
            <div>open</div>
          </div>
          <div className="w-full h-12 m-4 border-2 rounded-full">
            <div className="w-12 p-1 pl-3">
              <i class="fa fa-bookmark"></i>
            </div>
          </div>
          <div className="w-full h-12 m-4 border-2 rounded-full">
            <div className="w-12 p-1 pl-2">
              <i class="fa fa-pencil"></i>
            </div>
          </div>
          <div className="w-full h-12 m-4 border-2 rounded-full">
            <div className="w-12 p-1 pl-2">
              <i class="fa fa-trash"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Csr;
