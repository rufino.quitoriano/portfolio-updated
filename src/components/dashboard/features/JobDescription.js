import React from "react";
import johndoe from "./../components/image/johndoe.png";

function JobDescription() {
  return (
    <div className="mt-12 px-52">
      <div className="flex">
        <div className="w-3/5">
          <div className="flex items-center h-16 px-12 text-gray-100 bg-gray-600 rounded-xl">
            <div className="text-3xl">JOB DESCRIPTION</div>
            <div className="flex p-2 ml-4 text-xl bg-gray-500 rounded-xl">
              <div className="mr-2">
                <i class="fa fa-pencil"></i>
              </div>
              <div>EDIT</div>
            </div>
          </div>
          <div className="px-12 text-gray-500 ">
            <div className="flex justify-between pb-4 my-8 text-xl font-bold tracking-widest border-b-4">
              <div>POSITION TITLE</div>
              <div>Junior Web Developer</div>
            </div>
            <div className="flex justify-between pb-4 my-8 text-xl font-bold tracking-widest border-b-4">
              <div>LOCATION</div>
              <div>Makati Metro Manila, Ph.</div>
            </div>
            <div className="flex justify-between pb-4 my-8 text-xl font-bold tracking-widest border-b-4">
              <div>EMPLOYMENT TYPE</div>
              <div>Full Time</div>
            </div>
            <div className="flex justify-between pb-4 my-8 text-xl font-bold tracking-widest border-b-4">
              <div>EXPERIENCE</div>
              <div>Junior Level</div>
            </div>
            <div className="flex justify-between pb-8 my-8 text-sm font-bold tracking-widest border-b-4 md:pb-4 md:text-xl">
              <div>DESCRIPTION</div>
              <div className="pl-24">
                to assist the Lead Developer and work with his team in
                developing and maintaining these systems in accordance with the
                business needs. The Developer must act in manner which displays
                the utmost confidentiality and respect of information and pupil
                and staff records at all times.
              </div>
            </div>
            <div className="flex justify-between pb-8 my-8 text-xl font-bold tracking-widest border-b-4 md:pb-4">
              <div>HIRING lEAD</div>
              <div className="flex">
                <div className="w-10 h-10">
                  <img className="rounded-full" src={johndoe} />
                </div>
                <div className="pt-3 ml-4">John Doe</div>
              </div>
            </div>
            <div className="flex justify-between pb-8 my-8 text-xl font-bold tracking-widest border-b-4 md:pb-4">
              <div>APPROVED SALARY</div>
              <div>₱35,000</div>
            </div>
          </div>
        </div>
        <div className="w-2/5 px-6 md:px-32">
          <div>
            <div className="flex items-center justify-center h-16 px-2 text-2xl font-bold tracking-widest text-gray-100 bg-blue-600 rounded-xl">
              <div className="pr-12">
                <i class="fa fa-facebook-f"></i>
              </div>
              <div>Post to Facebook</div>
            </div>
            <div className="flex items-center justify-center h-16 px-2 my-4 text-2xl font-bold tracking-widest text-gray-100 bg-teal-400 rounded-xl">
              <div className="pr-14">
                <i class="fa fa-twitter"></i>
              </div>
              <div>Tweet this Job</div>
            </div>
            <div className="flex items-center justify-center h-16 px-2 my-4 text-2xl font-bold tracking-widest text-gray-100 bg-teal-400 rounded-xl">
              <div className="pr-12">
                <i class="fa fa-linkedin"></i>
              </div>
              <div>Post to LinkedIn</div>
            </div>
            <div className="my-4 text-2xl text-gray-600">Link to this Job</div>
            <div className="text-gray-600">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </div>
            <div className="flex my-4 border-2">
              <input
                className="w-full h-12"
                placeholder="http://loremippsum.dol/sit.amet"
              ></input>
              <button>
                <div className="flex items-center h-12 bg-teal-400">COPY</div>
              </button>
            </div>

            <div className="my-4 text-2xl text-gray-600">
              Use the Botton Widget
            </div>
            <div className="text-gray-600">
              This widget embeds the buttons for sharing the job for submitting
              a resume on your website.
            </div>
            <div className="flex my-4 border-2">
              <input
                className="w-full h-12"
                placeholder=" <script type text/javascript/> "
              ></input>
              <button>
                <div className="flex items-center h-12 bg-teal-400">COPY</div>
              </button>
            </div>

            <div className="my-4 text-2xl text-gray-600">
              Upload Resume by Email
            </div>
            <div className="text-gray-600">
              You can automatically upload multiple resumes to this job by
              emailing the address below
            </div>
            <div className="flex my-4 border-2">
              <input
                className="w-full h-12"
                placeholder="JohnDoe@gmail.com"
              ></input>
              <button>
                <div className="flex items-center h-12 bg-teal-400">COPY</div>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default JobDescription;
