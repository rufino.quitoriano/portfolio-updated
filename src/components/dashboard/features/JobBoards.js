import React from "react";
import circlegauge from "../components/image/circlegauge.gif";
import bargraph from "../components/image/bargraph.gif";
import linegraph from "../components/image/linegraph.gif";

function JobBoards() {
  return (
    <div className="px-52">
      <div>
        <div className="flex justify-between h-16 pt-8">
          <button>
            <div className="text-xl font-bold tracking-widest">SUMMARY</div>
          </button>
          <button>
            <div className="text-xl font-bold tracking-widest">APPLICANTS</div>
          </button>
          <button>
            <div className="text-xl font-bold tracking-widest">JOB BOARD</div>
          </button>
          <button>
            <div className="text-xl font-bold tracking-widest">INTERVIEWS</div>
          </button>
          <button>
            <div className="text-xl font-bold tracking-widest">ACTIVITY</div>
          </button>
          <button>
            <div className="text-xl font-bold tracking-widest">BROADCAST</div>
          </button>
          <button>
            <div className="text-xl font-bold tracking-widest">
              NOTIFICATION
            </div>
          </button>
        </div>
        <div className="flex justify-center px-80">
          <img src={circlegauge} />
          <img src={bargraph} />
          <img src={linegraph} />
        </div>
        <div className="h-4 mt-8 bg-gray-600"></div>
      </div>
    </div>
  );
}

export default JobBoards;
