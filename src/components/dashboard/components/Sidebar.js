import React from "react";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import * as IconName from "react-icons/io5";

export const SidebarData = [
  {
    title: "Home",
    icon: <AiIcons.AiFillHome />,
    cName: "nav-text",
    id: "#",
  },
  {
    title: "jobs",
    icon: <IoIcons.IoLogoSteam />,
    cName: "nav-text",
    id: "#skills",
  },
  {
    title: "Resume",
    icon: <IconName.IoLogoElectron />,
    cName: "nav-text",
    id: "#experience",
  },
  {
    title: "Task",
    icon: <IoIcons.IoIosPaper />,
    cName: "nav-text",
    id: "#recentworks",
  },
  {
    title: "Calendar",
    icon: <IconName.IoManSharp />,
    cName: "nav-text",
    id: "#aboutme",
  },
];
