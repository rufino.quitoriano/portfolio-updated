import React from "react";
import Csr from "../features/Csr";
import JobBoards from "../features/JobBoards";
import JobDescription from "../features/JobDescription";

function Body() {
  return (
    <div>
      <Csr />
      <JobBoards />
      <JobDescription />
    </div>
  );
}

export default Body;
