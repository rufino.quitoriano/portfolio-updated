import React, { useState } from "react";
import * as FaIcons from "react-icons/fa";
import { Link } from "react-router-dom";
import * as AiIcons from "react-icons/ai";
import { IconContext } from "react-icons";
import { SidebarData } from "./Sidebar";

function Navbar() {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  const sidebarClass = () => {
    const test2 = sidebar ? "nav-menu active" : "nav-menu";
    const test = "bg-gray-700 " + test2;

    return test;
  };

  return (
    <div className="w-full">
      <div className="flex items-center justify-between px-32 text-6xl text-center text-yellow-100 bg-teal-900">
        <div className="flex justify-end pt-4 text-2xl text-yellow-100 ">
          <Link to="/">
            <button className="items-center border-2 ">Back to HomePage</button>
          </Link>
        </div>
        <div>SAMPLE DASHBOARD</div>
        <div className="flex justify-end pt-4 text-2xl text-yellow-100 ">
          <Link to="/">
            <button className="border-2 ">Back to HomePage</button>
          </Link>
        </div>
      </div>
      <div className="px-32 mt-8">
        <IconContext.Provider value={{ color: "#525554" }}>
          <div className="border-2">
            <div className="flex p-1 bg-gray-100">
              <div>
                <Link to="#" className="menu-bars">
                  <FaIcons.FaBars onClick={showSidebar} />
                </Link>
              </div>
              <button className="w-12 p-2 mr-2">
                <div className="flex items-center justify-center">
                  <svg
                    fill="black"
                    height="19"
                    viewBox="0 0 19 19"
                    width="19"
                    class="shopee-svg-icon "
                  >
                    <g fill-rule="evenodd" stroke="none" stroke-width="1">
                      <g transform="translate(-1016 -32)">
                        <g>
                          <g transform="translate(405 21)">
                            <g transform="translate(611 11)">
                              <path d="m8 16c4.418278 0 8-3.581722 8-8s-3.581722-8-8-8-8 3.581722-8 8 3.581722 8 8 8zm0-2c-3.3137085 0-6-2.6862915-6-6s2.6862915-6 6-6 6 2.6862915 6 6-2.6862915 6-6 6z"></path>
                              <path d="m12.2972351 13.7114222 4.9799555 4.919354c.3929077.3881263 1.0260608.3842503 1.4141871-.0086574.3881263-.3929076.3842503-1.0260607-.0086574-1.414187l-4.9799554-4.919354c-.3929077-.3881263-1.0260608-.3842503-1.4141871.0086573-.3881263.3929077-.3842503 1.0260608.0086573 1.4141871z"></path>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                </div>
              </button>
              <input className="w-full" placeholder="Search"></input>
              <div className="flex px-2 text-5xl">
                <button>
                  <i class="fa fa-user-plus"></i>
                </button>
                <button>
                  <div className="px-2">
                    <i class="fa fa-tasks"></i>
                  </div>
                </button>
                <button>
                  <i class="fa fa-th"></i>
                </button>
              </div>
            </div>
          </div>
          <nav className={sidebarClass()}>
            <ul className="nav-menu-items" onClick={showSidebar}>
              <li className="navbar-toggle">
                <Link to="#" className="menu-bars">
                  <AiIcons.AiOutlineClose />
                </Link>
              </li>
              {SidebarData.map((item, index, id) => {
                return (
                  <li key={index} className={item.cName}>
                    <a href={item.id}>
                      {item.icon}
                      <span>{item.title}</span>
                    </a>
                  </li>
                );
              })}
            </ul>
          </nav>
        </IconContext.Provider>
      </div>
    </div>
  );
}

export default Navbar;
