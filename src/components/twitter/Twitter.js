import React from "react";
import Home from "./layers/Home";
import TwitNav from "./layers/TwitNav";
import "./styles/Twitter.css";

function Twitter() {
  return (
    <div>
      <TwitNav />
      <Home />
    </div>
  );
}

export default Twitter;
