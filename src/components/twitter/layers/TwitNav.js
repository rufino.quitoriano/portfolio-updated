import React from "react";
import { Link } from "react-router-dom";

function TwitNav() {
  return (
    <div>
      <div className="flex items-center justify-between w-full px-2 text-xl text-center text-yellow-100 bg-teal-700 md:text-6xl md:px-32">
        <div className="flex justify-end py-2 text-sm text-yellow-100 md:text-2xl ">
          <Link to="/">
            <button className="items-center border-2 ">Back to HomePage</button>
          </Link>
        </div>
        <div>SAMPLE TWITTER</div>
        <div className="flex justify-end py-2 text-sm text-yellow-100 md:text-2xl ">
          <Link to="/">
            <button className="border-2 ">Back to HomePage</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default TwitNav;
