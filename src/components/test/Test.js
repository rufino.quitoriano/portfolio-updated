import React from "react";
// import ThemeSwitcher from "react-theme-switcher";
import TestNight from "./TestNight";

const colors = {
  light: {
    background: "#fff",
    color: "#000",
  },
  dark: {
    background: "#000",
    color: "#fff",
  },
};
const activeMode = "light";
const offlineStorageKey = "react-random-key";

function Test() {
  return (
    <div>
      <div className="flex items-center justify-center bg-gray-400 text-9xl">
        hello
      </div>
      {/* <ThemeProvider> */}
      <TestNight />
      {/* </ThemeProvider> */}
    </div>
  );
}

export default Test;
