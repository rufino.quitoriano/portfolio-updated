import React, { useState } from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { Link } from "react-router-dom";
import { SidebarData } from "./Sidebar";
import "./Navbar.css";
import { IconContext } from "react-icons";
import logo from "../navbar/logo.png";
function Navbar() {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  const sidebarClass = () => {
    const test2 = sidebar ? "nav-menu active" : "nav-menu";
    const test = "bg-cyan-800 " + test2;

    return test;
  };

  return (
    <div className="fixed z-20 bg-cyan-800 ">
      <IconContext.Provider value={{ color: "#fff" }}>
        <div>
          <div className="flex justify-between w-screen md:w-screen">
            <div>
              <a href="#">
                <div className="flex items-center text-base text-yellow-100 md:text-4xl">
                  <div className="w-24 md:w-32">
                    <img src={logo} />
                  </div>
                  <div>Quitoriano, Rufino Jr.</div>
                </div>
              </a>
            </div>
            <div className="flex items-center">
              <div className="m-2 text-base text-yellow-100 md:m-8 md:text-4xl md:mr-8">
                <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                  <a
                    target="_blank"
                    href="https://www.facebook.com/otaku.zenith.1/"
                  >
                    <div className="flex">
                      <div>
                        <i class="fa fa-facebook"></i>
                      </div>
                    </div>
                  </a>
                </button>
              </div>

              <div className="m-2 text-base text-yellow-100 md:m-8 md:text-4xl md:mr-8">
                <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                  <a
                    target="_blank"
                    href="https://www.instagram.com/otakuzenith/"
                  >
                    <i class="fa fa-instagram"></i>
                  </a>
                </button>
              </div>

              <div className="m-2 text-base text-yellow-100 md:m-8 md:text-4xl md:mr-8">
                <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                  <a target="_blank" href="https://twitter.com/yukino33419124">
                    <i class="fa fa-twitter"></i>
                  </a>
                </button>
              </div>

              <div className="text-base text-yellow-100 md:m-2 md:text-4xl">
                <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                  <a
                    target="_blank"
                    href="https://www.linkedin.com/in/zenith-xd-b207641a2/"
                  >
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                  </a>
                </button>
              </div>

              <div className="flex items-center m-2 md:mx-16">
                <Link to="#" className="menu-bars">
                  <FaIcons.FaBars onClick={showSidebar} />
                </Link>
              </div>
            </div>
          </div>
        </div>
        <nav className={sidebarClass()}>
          <ul className="nav-menu-items" onClick={showSidebar}>
            <li className="navbar-toggle">
              <Link to="#" className="menu-bars">
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            {SidebarData.map((item, index, id) => {
              return (
                <li key={index} className={item.cName}>
                  <a href={item.id}>
                    {item.icon}
                    <span>{item.title}</span>
                  </a>
                </li>
              );
            })}
          </ul>
        </nav>
      </IconContext.Provider>
    </div>
  );
}

export default Navbar;
