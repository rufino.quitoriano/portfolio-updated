import React from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import * as IconName from "react-icons/io5";

export const SidebarData = [
  {
    title: "Home",
    icon: <AiIcons.AiFillHome />,
    cName: "nav-text",
    id: "#",
  },
  {
    title: "Skills",
    icon: <IoIcons.IoLogoSteam />,
    cName: "nav-text",
    id: "#skills",
  },
  {
    title: "Experience",
    icon: <IconName.IoLogoElectron />,
    cName: "nav-text",
    id: "#experience",
  },
  {
    title: "Recent Works",
    icon: <IoIcons.IoIosPaper />,
    cName: "nav-text",
    id: "#recentworks",
  },
  {
    title: "About Me",
    icon: <IconName.IoManSharp />,
    cName: "nav-text",
    id: "#aboutme",
  },
  {
    title: "Contacts",
    icon: <IoIcons.IoMdPeople />,
    cName: "nav-text",
    id: "#contacts",
  },
  {
    title: "Messages",
    icon: <FaIcons.FaEnvelopeOpenText />,
    cName: "nav-text",
    id: "#messages",
  },
];
