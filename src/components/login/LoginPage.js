import React from "react";
import { Link } from "react-router-dom";
import login from "../../images/login.jpg";

function LoginPage() {
  return (
    <div className="flex items-center justify-center">
      <div className="flex">
        <div className="flex flex-col md:flex-row">
          <div className="flex flex-row items-center w-full h-full px-4 md:w-1/2 md:px-16">
            <div>
              <div className="pt-6 md:m-4">
                <button>
                  <div className="flex items-center justify-center w-20 h-16 font-serif text-2xl font-bold text-gray-100 bg-gray-900">
                    logo
                  </div>
                </button>
              </div>
              <div className="px-2 pt-4 md:px-36">
                <div className="pb-4 font-serif text-3xl font-bold tracking-widest text-center md:pb-12 md:text-8xl">
                  Welcome.
                </div>
              </div>
              <div className="px-0 md:px-16">
                <div className="mt-2 text-3xl font-bold tracking-widest md:mt-6">
                  Email
                </div>
                <div className="text-xl border-2 rounded">
                  <input
                    className="h-8 p-4 leading-tight focus:outline-none focus:shadow-outline"
                    type="email"
                    id="email"
                    placeholder="your@email.com"
                  ></input>
                </div>
                <div className="mt-6 text-3xl font-bold tracking-widest">
                  Password
                </div>
                <div className="text-xl border-2 rounded">
                  <input
                    className="h-8 p-4 leading-tight focus:outline-none focus:shadow-outline"
                    placeholder="Password"
                  ></input>
                </div>
                <div>
                  <div>
                    <input
                      type="submit"
                      value="Log In"
                      className="w-full p-2 mt-8 text-lg font-bold text-gray-100 bg-gray-900 hover:bg-gray-700"
                    ></input>
                  </div>
                </div>
                <div className="flex justify-center">
                  <div className="flex pt-8">
                    <p className="pr-2">Don't have an account?</p>
                    <p className="font-semibold underline">
                      <Link to="/login/register">Register here.</Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full md:w-1/2 ">
            <img
              className="hidden w-full h-96 md:h-full md:block"
              src={login}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;
