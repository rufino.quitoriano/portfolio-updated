import React from "react";
import { Switch, Route } from "react-router-dom";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";

function Login({ match }) {
  console.log(match);
  return (
    <div>
      <Switch>
        <Route path={match.url + "/register"} component={RegisterPage}></Route>
        <Route path={match.url + "/"} component={LoginPage}></Route>
      </Switch>
    </div>
  );
}

export default Login;
