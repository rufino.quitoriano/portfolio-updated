import React from "react";
import { Link } from "react-router-dom";
import Body from "./components/layers/Body";
import Footer from "./components/layers/Footer";
import Navbar from "./components/layers/Navbar";
import "./components/styles/hackreactor.css";

function Hackreactor() {
  return (
    <div className="absolute">
      <div className="flex items-center justify-between px-2 text-base text-center text-yellow-100 bg-teal-900 md:w-screen md:text-6xl md:px-32">
        <div className="flex items-center text-2xl text-yellow-100 ">
          <Link to="/">
            <button className="text-xs border-2 md:text-4xl ">
              Back to HomePage
            </button>
          </Link>
        </div>
        <div className="text-xl md:text-6xl">SAMPLE HACK REACTOR</div>
        <div className="flex text-2xl text-yellow-100 ">
          <Link to="/">
            <button className="text-xs border-2 md:text-4xl">
              Back to HomePage
            </button>
          </Link>
        </div>
      </div>
      <Navbar />
      <Body />
      <Footer />
    </div>
  );
}

export default Hackreactor;
