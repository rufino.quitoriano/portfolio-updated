import React from "react";
import crs from "../components/images/crs.png";
import star from "../components/images/star.png";
import ck from "../components/images/ck.png";
import google from "../components/images/google.png";
import BestISA from "./features/BestISA";

function Header() {
  return (
    <div>
      <div className="flex flex-col items-center justify-center min-h-screen bg">
        <div className="flex flex-col items-center justify-center text-gray-200">
          <div className="font-serif text-4xl text-center lg:text-7xl">
            The Most Advanced Coding Bootcamp: Think
            <br /> Like a Software Engineer
          </div>
          <div className="items-center justify-center mt-5 text-xl text-center md:text-3xl">
            Don't just learn to code.<b>Reinvent your career</b>
            &nbsp;full-time, part-time, in-person or online.
          </div>
          <button className="w-64 h-16 mt-20 bg-blue-500 rounded-lg">
            Start Learning for Free
          </button>
        </div>
      </div>
      <div className="flex justify-center h-16 px-2 bg-gray-300 md:h-32">
        <div className="flex flex-col items-center justify-center py-4">
          <div className="text-xs text-center md:text-2xl">
            Rated “Best Coding”
          </div>
          <div className="flex justify-center">
            <img className="w-32 md:w-96" src={crs} />
          </div>
        </div>
        <div className="flex flex-col items-center justify-center px-2 py-4">
          <div className="text-xs text-center md:text-2xl">Quora Reviews</div>
          <div className="flex justify-center">
            <img className="w-32 md:w-96" src={star} />
          </div>
        </div>
        <div className="flex flex-col items-center justify-center px-2 py-4">
          <div className="text-xs text-center md:text-2xl">
            Rated “Best ISA”
          </div>
          <div className="flex justify-center">
            <img className="w-20 md:w-64" src={ck} />
          </div>
        </div>
        <div className="flex flex-col items-center justify-center px-2 py-4">
          <div className="text-xs text-center md:text-2xl">
            Over 8,000 Alumni
          </div>
          <div className="flex justify-center">
            <img className="w-20 md:w-64" src={google} />
          </div>
        </div>
      </div>
      <div className="w-full h-4 bg-blue-500"></div>
      <BestISA />
    </div>
  );
}

export default Header;
