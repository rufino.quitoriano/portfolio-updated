import React from "react";

function GetStarted() {
  return (
    <div className="w-full p-5 text-center">
      <h1 className="font-serif text-5xl text-gray-900">
        Get Started With Hack Reactor
      </h1>
      <button className="h-16 p-2 px-10 mt-5 text-2xl text-white bg-blue-500 lg:px-4 hover:bg-blue-400">
        Learn More about our Admissions Process
      </button>
    </div>
  );
}

export default GetStarted;
