import React from "react";
import BootcampCard from "./features/BootcampCard";
import Syllabus from "./features/Syllabus";
import Testimonies from "./features/Testimonies";

function OurBootcamp() {
  return (
    <div>
      <div className="h-full px-4 md:px-96">
        <h1 className="mt-10 font-serif text-5xl text-center text-gray-900">
          Our Bootcamps
        </h1>
        <div>
          <BootcampCard />
          <Syllabus />
          <Testimonies />
        </div>
      </div>
    </div>
  );
}

export default OurBootcamp;
