import React from "react";
import pics1 from "../components/images/pics1.png";
import pics2 from "../components/images/pics2.png";
import pics3 from "../components/images/pics3.png";
function Attention() {
  return (
    <div className="flex justify-center px-4 md:px-48">
      <div>
        <div>
          <div className="hidden mt-10 font-serif text-3xl text-center text-gray-900 lg:block">
            What's Getting Attention at Hack Reactor?
          </div>
          <div className="flex justify-center">
            <div className="flex flex-col items-center justify-center w-4/5 mt-10 shadow-lg lg:flex-row">
              <div>
                <img className="w-96" src={pics1} />
              </div>
              <div className="flex flex-col items-center justify-center">
                <h1 className="font-serif text-xl text-gray-900">
                  Galvanize Names Harsh Patel as Chief Executive Officer
                </h1>
                <p className="px-16 py-4 text-xl">
                  Galvanize, the leader in technology workforce education,
                  announced today that Harsh Patel has been named CEO of
                  Galvanize. Karl Maier also joins as Executive Chairman. Former
                  CEO, Al Rosabal, will be stepping down as CEO after making key
                  additions to the executive leadership team.
                </p>
              </div>
              <div className="flex justify-center w-full lg:w-1/4">
                <button className="w-40 h-10 my-10 text-blue-400 border border-blue-400">
                  Read More
                </button>
              </div>
            </div>
          </div>
          <div className="flex justify-center my-10">
            <div className="flex flex-col items-center justify-center w-4/5 shadow-lg lg:flex-row">
              <div>
                <img className="w-96" src={pics2} />
              </div>
              <div className="flex flex-col items-center justify-center">
                <h1 className="font-serif text-xl text-gray-900">
                  Top 6 jobs that can be done remotely
                </h1>
                <p className="px-16 py-4 text-xl ">
                  If you’ve always wanted to work from home, there’s never been
                  a better time to start thinking about a new career. One of my
                  favorite teachers of all time was the lead instructor of Hack
                  Reactor at Austin.position I wanted. Galvanize's careeer
                  services and support have far surpassed my expectations"
                </p>
              </div>
              <div className="flex justify-center w-full lg:w-1/4">
                <button className="w-40 h-10 my-10 text-blue-400 border border-blue-400">
                  Read More
                </button>
              </div>
            </div>
          </div>
          <div className="flex justify-center">
            <div className="flex flex-col items-center justify-center w-4/5 shadow-lg lg:flex-row">
              <div>
                <img className="w-96" src={pics3} />
              </div>
              <div className="flex flex-col items-center justify-center">
                <h1 className="font-serif text-xl text-gray-900">
                  Take Hack Reactor Courses at Galvanize Campuses
                </h1>
                <p className="px-16 py-4 text-xl">
                  Hack Reactor’s top coding bootcamps are now taught at
                  Galvanize campuses in Austin, Boulder, Denver, Los Angeles,
                  New York, Phoenix, San Francisco, and Seattle (as well as
                  online). Galvanize campuses are unique technology ecosystems
                  designed to support your learning journey while providing
                  access to professional,
                </p>
              </div>
              <div className="flex justify-center w-full lg:w-1/4">
                <button className="w-40 h-10 my-10 text-blue-400 border border-blue-400">
                  Read More
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Attention;
