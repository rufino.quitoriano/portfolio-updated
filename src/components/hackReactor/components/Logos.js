import React from "react";

function Logos() {
  return (
    <div>
      <div className="px-4 mt-10 text-center md:px-48">
        <h1 className="font-serif text-xl text-gray-900 md:text-5xl">
          "Hack Reactor Grads are Our First Choice"
        </h1>
        <p className="hidden mt-5 text-2xl lg:block">
          The best coding bootcamps simulate a real-world software environment
          for their students; the grads are so well-prepared, they <br /> come
          in and hit the ground running, that's really what we're looking for.
        </p>
        <p className="text-xl md:text-2xl">
          <i>- Dustin B., Cisco</i>
        </p>
        <div className="flex flex-wrap items-center justify-between mt-8 lg:justify-evenly ">
          <div className="w-1/4 m-2 h-36 lg:w-1/6 fb"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 li"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 go"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 pp"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 ap"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 am"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 co"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 mi"></div>
          <div className="w-1/4 m-2 h-36 lg:w-1/6 vi"></div>
          <div className="hidden w-1/4 m-2 h-36 lg:w-1/6 md:block ac"></div>
        </div>
      </div>
    </div>
  );
}

export default Logos;
