import React from "react";
import Social from "./Social";

function Infos() {
  return (
    <div className="flex items-center justify-between text-center md:text-left">
      <div>
        <div className="font-bold">GET STARTED</div>
        <div className="mt-5">Admissions Process</div>
        <div className="mt-5">Live Prep Course</div>
        <div className="mt-5">Scholarships</div>
        <div className="mt-5">Tour</div>
      </div>

      <div>
        <div className="font-bold">COMPANY</div>
        <div className="mt-5">About</div>
        <div className="mt-5">Careers</div>
        <div className="mt-5">Blog</div>
        <div className="mt-5">Regulatory Information</div>
      </div>

      <div>
        <div className="font-bold">MORE</div>
        <div className="mt-5">Partnerships</div>
        <div className="mt-5">FAQ</div>
        <div className="mt-5">Enterprise</div>
        <div className="mt-5">Live</div>
      </div>

      <div>
        <div className="flex flex-col">
          <div className="font-bold">REVIEWS</div>
          <div className="flex">
            <div className="m-2">Google</div>
            <div className="m-2">SwitchUp</div>
            <div className="m-2">Yelp</div>
            <div className="m-2">Course Report</div>
          </div>
        </div>
        <div>
          <div className="mt-5 font-bold lg:mt-auto">SOCIAL</div>
          <div>
            <Social />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Infos;
