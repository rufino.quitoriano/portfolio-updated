import React from "react";

function BestISA() {
  return (
    <div className="w-full px-8 bg-gray-500">
      <div className="flex flex-col items-center justify-start">
        <div className="py-8 text-xl text-gray-900 md:text-4xl">
          We now have the best ISA in the industry
        </div>
        <div className="text-sm text-left text-gray-900 md:text-2xl">
          In response to financial uncertainty, we've updated our income share
          agreement terms to be even more <br /> accomodating for our students
          through the end of 2020.
        </div>
        <button className="w-48 h-16 my-5 text-2xl text-white bg-blue-500 hover:bg-blue-400">
          Learn more
        </button>
      </div>
    </div>
  );
}

export default BestISA;
