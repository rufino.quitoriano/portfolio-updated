import React from "react";

function Testimonies() {
  return (
    <div className="">
      <div className="flex px-32 mt-10 ">
        <div className="w-1/2 lg:w-3/4">
          <div className="flex flex-col max-w-2xl p-5 m-5 shadow-lg lg:p-0 lg:flex-row">
            <div className="w-full h-64 lg:h-auto lg:w-1/4 t1"></div>
            <div className="w-full p-5 lg:w-2/3">
              <div>
                "I just accepted an offer from Amex to be a software engineer in
                New York, and it is exactly the type of position I wanted.
                Galvanize's careeer services and support have far surpassed my
                expectations"
              </div>
              <i>Melissa Louie, Software Engineer, American Express</i>
            </div>
          </div>
          <div className="flex flex-col max-w-2xl p-5 m-5 shadow-lg lg:p-0 lg:flex-row">
            <div className="w-full h-64 lg:h-auto lg:w-1/4 t2"></div>
            <div className="w-full p-5 lg:w-2/3">
              <div>
                "When the bootcamp believed in me by offering the opportunity
                for an Income Share Agreement, then I believed in the bootcamp."
              </div>
              <i>Kevin Fang, Software Engineer, Self-Employed</i>
            </div>
          </div>
          <div className="flex flex-col max-w-2xl p-5 m-5 shadow-lg lg:p-0 lg:flex-row">
            <div className="w-full h-64 lg:h-auto lg:w-1/4 t3"></div>
            <div className="w-full p-5 lg:w-2/3">
              <div>
                "I've been around teachers for a really long time, both in a
                collegial sense and in a student-teacher type of relationship.
                One of my favorite teachers of all time was the lead instructor
                of Hack Reactor at Austin."
              </div>
              <i>
                Evelyn Binkard, Software Engineer, Kapsch Trafficcom North
                America
              </i>
            </div>
          </div>
        </div>
        <div className="flex flex-col items-center justify-between w-1/2 py-20 m-5 shadow-xl md:py-0">
          <div className="w-32 h-32 my-20 md:my-2 a1"></div>
          <div className="w-32 h-32 my-20 md:my-2 a2"></div>
          <div className="w-32 h-32 mt-20 md:my-2 a3"></div>
          <div className="m-4">
            <div className="text-center">
              Career Karma, Switchup, and Course Report all consider Hack
              Reactor a top bootcamp in several categories.
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Testimonies;
