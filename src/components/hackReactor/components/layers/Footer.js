import React from "react";
import Stay from "../Stay";

function Footer() {
  return (
    <div>
      <div className="w-full px-4 py-10 bg-blue-500">
        <Stay />
      </div>
      <div className="h-20 text-white bg-blue-600 opacity-80">
        <div className="flex items-center justify-between h-full max-w-5xl mx-auto">
          <p>© 2020 Galvanize. All rights reserved.</p>
          <div className="flex text-black">
            <p className="hover:text-white">Terms &nbsp;</p>
            <p className="hover:text-white">/ Privacy Policy</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
