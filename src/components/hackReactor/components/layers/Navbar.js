import React from "react";
import hr from "../images/hr.png";
function Navbar() {
  return (
    <div>
      <div className="flex items-center justify-center w-full h-16 md:h-32">
        <div className="flex items-center justify-center w-full">
          <div>
            <img className="w-64" src={hr} />
          </div>
          <div>
            <div className="flex mx-2 text-xs text-gray-700 md:mx-4 md:text-2xl hover:text-blue-400">
              <div className="mr-1">Bootcamps</div>
              <div className="mt-1">
                <i class="fa fa-caret-down" />
              </div>
            </div>
          </div>
          <div>
            <div className="flex mx-2 text-xs text-gray-700 md:mx-4 md:text-2xl hover:text-blue-400">
              <div className="mr-1">Professional Development</div>
              <div className="mt-1">
                <i class="fa fa-caret-down" />
              </div>
            </div>
          </div>

          <div>
            <div className="flex mx-2 text-xs text-gray-700 md:mx-4 md:text-2xl hover:text-blue-400">
              <div className="mr-1">Prepare</div>
              <div className="mt-1">
                <i class="fa fa-caret-down" />
              </div>
            </div>
          </div>
          <div>
            <div className="flex mx-2 text-xs text-gray-700 md:mx-4 md:text-2xl hover:text-blue-400">
              <div className="mr-1">Outcomes</div>
              <div className="mt-1">
                <i class="fa fa-caret-down" />
              </div>
            </div>
          </div>
          <div>
            <div className="flex mx-2 text-xs text-gray-700 md:mx-4 md:text-2xl hover:text-blue-400">
              <div className="mr-1">Events</div>
              <div className="mt-1">
                <i class="fa fa-caret-down" />
              </div>
            </div>
          </div>
          <button className="w-32 h-12 text-xs text-white bg-blue-500 md:text-base rounded-xl hover:bg-blue-400">
            <div>Apply Now</div>
          </button>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
