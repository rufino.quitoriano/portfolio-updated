import React from "react";
import Attention from "../Attention";
import GetStarted from "../GetStarted";
import Header from "../Header";
import Logos from "../Logos";
import News from "../News";
import OurBootcamp from "../OurBootcamp";
import Video from "../Video";

function Body() {
  return (
    <div className="w-full">
      <Header />
      <Logos />
      <Video />
      <OurBootcamp />
      <Attention />
      <News />
      <GetStarted />
    </div>
  );
}

export default Body;
