import React from "react";

function FlashDeals() {
  return (
    <div className="w-full px-12 md:px-32">
      <div className="flex items-center h-32 px-2 mt-0 text-xl font-bold text-gray-900 md:mt-4">
        <div>FLASH DEALS</div>
      </div>
      <div className="flex items-center justify-between">
        <div className="w-full m-2">
          <img src="https://cf.shopee.ph/file/587fd56b27b107523bb346778c4fc3d4_tn" />
        </div>
        <div className="w-full m-2">
          <img src="https://cf.shopee.ph/file/9951bf2b33cdeed066b9b76e39ce22b1_tn" />
        </div>
        <div className="w-full m-2">
          <img src="https://cf.shopee.ph/file/cc9c546dd75febad2ec38448673b4608_tn" />
        </div>
        <div className="w-full m-2">
          <img src="https://cf.shopee.com.my/file/41e4f5ef56443506a414224510591750" />
        </div>
        <div className="w-full m-2">
          <img src="https://cf.shopee.ph/file/834fe777bdf54d2e7be819c7b21fc39f_tn" />
        </div>
      </div>
      <div className="flex items-center justify-center h-12 mt-8 rounded-full md:mt-0 md:h-32">
        <div>
          <img src="https://cf.shopee.ph/file/5a6222308308dc1915bce28a0d091d6b" />
        </div>
        <div>
          <img src="https://cf.shopee.ph/file/9eb526e9342a9f9eca25a6ddf47a7dbf" />
        </div>
        <div>
          <img src="https://cf.shopee.ph/file/728c47406f614f5b1381adb9ed2631d7" />
        </div>
      </div>
    </div>
  );
}

export default FlashDeals;
