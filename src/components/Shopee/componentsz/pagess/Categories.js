import React from "react";

function Categories() {
  return (
    <div>
      <div className="w-full px-12 md:px-32">
        <div className="flex items-center justify-start h-6 mt-2 text-xl font-bold md:mt-8 md:h-16">
          CATEGORIES
        </div>
        <div className="mt-2">
          <div>
            <div className="flex items-center justify-between pt-4 md:px-20">
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/2e9bfe13ce9cecfbfad8010b843651f6_tn" />
                <div className="flex">
                  <div className="pr-1 text-xs md:text-base md:pr-2">Men's</div>
                  <div className="text-xs md:text-base">Apparel</div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/907fd63b68fa6b779d30b2a7fbf35a75_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Mobile Gadget's
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/789d62a7d2846cabeded9735484788bd_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Mobile Accessories
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/f49a602bc6dce49d5c99483fa6658c74_tn" />
                <div className="flex flex-col text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">Home</div>
                  <div className="text-xs md:text-base">Entertainment</div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/4cb60a60af9732410ba3b9d99cca58fa_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Babies & kids
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/5ad0c09e955d2c99869e046d0549ba81_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Home & living
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/678f3bd69c920bb0640b6a800ccc9c49_tn" />
                <div>
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Grocerie's
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/09830879bce51190f895807a1d38c087_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    toy's & Colectibles
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/55267466ff3f0445bf28c21ddafd409c_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Women's Bags
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/9ab43516f83df1b1bc400edf56bccac7_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Women's Accesories
                  </div>
                </div>
              </div>
            </div>

            <div className="flex items-center justify-between pt-20 md:px-20">
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/372cf1ccdb799772760d819408df35ba_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Women's Apparel
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/346dfb2914e720d94ba6f5cc790b9583_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Health & Personal Care
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/adc26d67694456ca66600dadaeb91805_tn" />
                <div className="flex text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Makeup & Fragrances
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/a8dbafc1ade925856d8634497ad2dfef_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Home Appliances
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/8fd4a7fb5f6758fbd2301e5a80388016_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Laptop & Computers
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/b63c10b71a2725c5c627933c0b35fecf_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Camera's
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/f12cfed5944ad868e181c6583a5a4426_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Sports & Travel
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/a3db43790bd5a473e606076fa4bc6717_tn" />
                <div className="text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Men's Accesories
                  </div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/4485e4fb0ed0372934c6e11ec3601d7c_tn" />
                <div className="pr-1 text-xs text-center md:pr-2 md:text-base">
                  <div>Men's Shoes</div>
                </div>
              </div>
              <div className="flex flex-col items-center justify-center w-20 h-20">
                <img src="https://cf.shopee.ph/file/b6a3f26a4beda468e99e8f73c9b3d77f_tn" />
                <div className="flex text-center">
                  <div className="pr-1 text-xs md:pr-2 md:text-base">
                    Motor's & Accesories
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Categories;
