import React from "react";
import ItemCard from "../pagess/ItemCard";

function DailyDiscover() {
  return (
    <div className="px-12 md:mt-2 md:px-32">
      <div>
        <div className="flex items-center px-8 py-8 font-semibold text-gray-900 md:h-64">
          DAILY DISCOVER
          <img
            className="w-10 h-10 m-8"
            src="https://image.flaticon.com/icons/png/512/2869/2869962.png"
          ></img>
        </div>
      </div>

      <div>
        <div className="flex justify-between">
          <div className="text-red-500"></div>

          <ItemCard
            title=" Women's One piece swimwear"
            price="₱349"
            numberOfSold="48 sold"
            photo="https://cf.shopee.ph/file/1572782cab5bd1967c4ff8cfb6d280f8"
          />

          <ItemCard
            title="Mobile Load Globe Autoload max 10"
            price="₱7.00"
            numberOfSold="9.6M"
            photo="https://cf.shopee.ph/file/abe0d212028d777a720c39020c05ec1b"
          />
          <ItemCard
            title="YSABELL Slide String Cotton Mini Dress"
            price="₱99.00"
            numberOfSold="27.3k"
            photo="https://cf.shopee.ph/file/10fdfcc121105e1c086e37442137ec9f"
          />
          <ItemCard
            title="(COD) 9 Colors TWS Bluetooth Earphone i12"
            price="₱176.00"
            numberOfSold="343.12k"
            photo="https://cf.shopee.ph/file/608e674557bfd387f366226aa303f79d"
          />
          <ItemCard
            title="SK Korean fashion flats sandals"
            price="₱66.00"
            numberOfSold="66.5k"
            photo="https://cf.shopee.ph/file/5c5189c8cfcd49f1dc2aabc63c9063db"
          />
          <ItemCard
            title="ZARA Fashion Knitted Ribbed Croptop"
            price="₱55.00"
            numberOfSold="55.6k"
            photo="https://cf.shopee.ph/file/79945c2f6def67690cdc30871a05deca"
          />
        </div>
      </div>

      <div>
        <div className="flex justify-between">
          <div className="text-red-500"></div>
          <ItemCard
            title="Korea Small Fragrance Ins Sexy Bikini"
            price="₱599.00"
            numberOfSold="118"
            photo="https://cf.shopee.ph/file/9f8f18c1e7da838c81fe923313d2c6d7"
          />
          <ItemCard
            title="Heng De Face Shield Protective"
            price="₱6.00"
            numberOfSold="71.8k"
            photo="https://cf.shopee.ph/file/4f54756f440efbee5383d2e5928e98ce"
          />
          <ItemCard
            title="Rachelle Two Piece With Pads"
            price="₱260.00"
            numberOfSold="160"
            photo="https://cf.shopee.ph/file/c4f673c124bf26c7cb9da8063b611971"
          />
          <ItemCard
            title="Taslan Shorts Unisex Sports Fashion Men"
            price="₱75.00"
            numberOfSold="183.4k"
            photo="https://cf.shopee.ph/file/d18e7eb7adac9b8be87b9587ff5ea980"
          />
          <ItemCard
            title="New Summer Two Strap Rubber Slippers"
            price="₱83.00"
            numberOfSold="114.7k"
            photo="https://cf.shopee.ph/file/7382fe172372bc1a06fcd4487e60cd28"
          />
          <ItemCard
            title="New Arival One Piece Swimwear"
            price="₱299.00"
            numberOfSold="6"
            photo="https://cf.shopee.ph/file/ce5dbf27a69ff536f8f01a1911a1442c"
          />
        </div>
      </div>
      <div>
        <div className="flex justify-between">
          <div className="text-red-500"></div>
          <ItemCard
            title="Kathryn High Quality Lettuce Top11118#"
            price="₱18.00"
            numberOfSold="3.7k"
            photo="https://cf.shopee.ph/file/6d973dbe06a12049bb2e400bc231634d"
          />
          <ItemCard
            title="Shorts For Unisex Sports Fashion Men"
            price="₱50.00"
            numberOfSold="191.4k"
            photo="https://cf.shopee.ph/file/77dd2cbfa6969aea7c60ee7ca0c363ac"
          />
          <ItemCard
            title="SALE DIY Self Adhesive 3D Brick Wall"
            price="₱25.00"
            numberOfSold="539.7k"
            photo="https://cf.shopee.ph/file/ef432c9a598dbdc478f7a4870c4da647"
          />
          <ItemCard
            title="MOM Jeans / Boyfriend Pants Highwaist"
            price="₱280.00"
            numberOfSold="70.1k"
            photo="https://cf.shopee.ph/file/9ceff8db6432e609ccdbd96e7ac85d80"
          />
          <ItemCard
            title="Window Curtain Decoration Assorted"
            price="₱60.00"
            numberOfSold="425.5k"
            photo="https://cf.shopee.ph/file/a1e03d88fbeda8a86a03bcbfe8ab2ba7"
          />
          <ItemCard
            title="Facial Mask Skin Care Korean Style"
            price="₱5.00"
            numberOfSold="439.1k"
            photo="https://cf.shopee.ph/file/10754247d8f0cf58f223834cb4a10de3"
          />
        </div>
      </div>

      <div>
        <div className="flex justify-between">
          <div className="text-red-500"></div>
          <ItemCard
            title="Mens Fashion Sports Shorts/Jogger"
            price="₱65.00"
            numberOfSold="233.5k"
            photo="https://cf.shopee.ph/file/ac70ab2c6ce54f2b58d8b9aee58a0bce"
          />
          <ItemCard
            title="6 Color Trendy Basketball Shorts For Men"
            price="₱79.00"
            numberOfSold="54.5k"
            photo="https://cf.shopee.ph/file/aad4ebae36589b97bb6cab32bbf1a5ae"
          />
          <ItemCard
            title="Face Mask Surgical 3ply 50pcs"
            price="₱88.00"
            numberOfSold="119.5k"
            photo="https://cf.shopee.ph/file/4c2a6333a06dfda6817b7c680725366f"
          />
          <ItemCard
            title="Sweat Shorts Unisex Plain With 2 Pocket"
            price="₱65.00"
            numberOfSold="30.2k"
            photo="https://cf.shopee.ph/file/53df3c4eabef80fea913df4e4fc93e36"
          />
          <ItemCard
            title="Disposable Face Mask 3ply Breatable"
            price="₱55.00"
            numberOfSold="141.3k"
            photo="https://cf.shopee.ph/file/b827d1c9f6aa7977d7693e7a7162a42e"
          />
          <ItemCard
            title="HTC Butterfly Curtain for Window"
            price="₱30.00"
            numberOfSold="20.6k"
            photo="https://cf.shopee.ph/file/e9a177c5b84854c91b6c1e25a7bc6870"
          />
        </div>
      </div>

      <div>
        <div className="flex justify-between">
          <ItemCard
            title="Whale Shark #S3000 Shorts for Unisex"
            price="₱89.00"
            numberOfSold="90.9k"
            photo="https://cf.shopee.ph/file/efa382a00b78115975e3a8c76c31e7c9"
          />
          <ItemCard
            title="COD Amelia One Piece Ruffled"
            price="₱259.00"
            numberOfSold="4"
            photo="https://cf.shopee.ph/file/2b5c3255430a3f56e9e7016a39b2cdd7"
          />
          <ItemCard
            title="MT 1 Piece Plain Canon Bath"
            price="₱30.00"
            numberOfSold="20.6k"
            photo="https://cf.shopee.ph/file/e9a177c5b84854c91b6c1e25a7bc6870"
          />
          <ItemCard
            title="Plain Shorts for Unisex Sports Fashion Men"
            price="₱55.00"
            numberOfSold="3.7k"
            photo="https://cf.shopee.ph/file/ef57bf4724b75f38353ea78ff6c711a7"
          />
          <ItemCard
            title="(COD) Ladyfair Korean Fashion Square Collar"
            price="₱198.00"
            numberOfSold="1.7k"
            photo="https://cf.shopee.ph/file/3930cef105403c1c331903b6b6d3b695"
          />
          <ItemCard
            title="SAT Women's Fashion Breatable"
            price="₱129.00"
            numberOfSold="200k"
            photo="https://cf.shopee.ph/file/0ed8de99864ab72a97a37eb0134ee058"
          />
        </div>
      </div>

      <div>
        <div className="flex justify-between">
          <div className="text-red-500"></div>
          <ItemCard
            title="SS Korean Fashion Garter Crop Top Slim"
            price="₱69.00"
            numberOfSold="107.8k"
            photo="https://cf.shopee.ph/file/009deaed4c7f222b8dbd6ccdb23b86a2"
          />
          <ItemCard
            title=" {With Glue+Gift} 24pcs. DIY Fake Nails French"
            price="₱38.00"
            numberOfSold="69.6k"
            photo="https://cf.shopee.ph/file/b499bfb5df8fd5d7e72bfc24dd5f77e1"
          />
          <ItemCard
            title="BASHA Puff Sleeves Smocked Top"
            price="₱99.00"
            numberOfSold="117.2k"
            photo="https://cf.shopee.ph/file/95a5a9c0a8d6972dac6cd7fe659b1c50"
          />
          <ItemCard
            title="RHian Womens Seamless Underwear"
            price="₱18.00"
            numberOfSold="388.4k"
            photo="https://cf.shopee.ph/file/bf6cb0d689465712a01d54d60514908b"
          />
          <ItemCard
            title="SEAN N95 Face Mask Protective"
            price="₱05.00"
            numberOfSold="929.6k"
            photo="https://cf.shopee.ph/file/6811a0a7af037893c6240bea40f97b8c"
          />
          <ItemCard
            title="SS TASLAN Shorts For Men Jogger Sweat"
            price="₱85.00"
            numberOfSold="27.7k"
            photo="https://cf.shopee.ph/file/7e321f34a2b007d1766048aab37f43b2"
          />
        </div>
      </div>
      <div className="w-full h-16"></div>
      <div className="flex items-center justify-center h-16 pb-20 border-b-4 border-red-500">
        <div className="w-1/3">
          <button className="flex items-center justify-center w-full h-10">
            See More
          </button>
        </div>
      </div>
    </div>
  );
}

export default DailyDiscover;
