import React, { useState } from "react";

function ItemCard({ title, price, numberOfSold, photo }) {
  const [cardhover, setCardHover] = useState(false);
  const hoverStyle = (cardhover) => {
    let hover = cardhover ? "block" : "hidden";
    console.log(hover);
    return hover + " border px-2 py-1 bg-orange-500";
  };
  return (
    <div
      className="w-1/6"
      onMouseEnter={() => setCardHover(true)}
      onMouseLeave={() => setCardHover(false)}
    >
      <div>
        <div className="flex flex-col justify-end h-full py-8 m-2 border-2 hover:border-red-500">
          <img src={photo} />
          <div>{title}</div>
          <div className="flex justify-between">
            <div className="text-red-500">{price}</div>
            <div>{numberOfSold}</div>
          </div>
        </div>
      </div>
      <div className={hoverStyle(cardhover)}>
        <button className="w-full">
          <div className="flex justify-center w-full bg-red-600 rounded-xl">
            <div>Find Similar</div>
          </div>
        </button>
      </div>
    </div>
  );
}

export default ItemCard;
