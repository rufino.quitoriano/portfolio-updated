import React from "react";
import shopeepay from "../../../../images/shopeepay.png";
import dragonpay from "../../../../images/dragonpay.png";
import shopeexpress from "../../../../images/shopeexpress.png";
import gogo from "../../../../images/gogo.png";
import entrego from "../../../../images/entrego.png";
import ninja from "../../../../images/ninja.svg";
import jnt from "../../../../images/jnt.png";
import lbc from "../../../../images/lbc.png";
import togo from "../../../../images/togo.png";
import xde from "../../../../images/xde.png";
import zoom from "../../../../images/zoom.png";
import mlhuillier from "../../../../images/mlhuillier.png";
import app from "../../../../images/app.png";

function Footer() {
  return (
    <div>
      {/* Footer */}
      <div className="w-full px-12 md:px-32">
        <div className="flex w-full">
          <div className="w-1/5 text-xs md:text-sm">
            <div className="pb-2 font-semibold">CUSTOMER SERVICE</div>
            <div>Help Centre</div>
            <div>Payment Methods</div>
            <div>ShopeePay</div>
            <div>Shopee Coins</div>
            <div>Order Tracking</div>
            <div>Free Shipping</div>
            <div>Return & Refund</div>
            <div>Shopee Guarantee</div>
            <div>Overseas Product</div>
            <div>Contact Us</div>
            <div></div>
          </div>
          <div className="w-1/5 text-xs md:text-sm">
            <div className="font-semibold">ABOUT SHOPEE</div>
            <div>About Us</div>
            <div>Shopee Blog</div>
            <div>Shopee Careers</div>
            <div>Shopee Policies</div>
            <div>Privacy Policy</div>
            <div>Shopee Mall</div>
            <div>Seller Centre</div>
            <div>Flash Deals</div>
            <div>Media Contact</div>
          </div>

          <div className="w-1/5 h-64">
            <div className="text-sm font-semibold">PAYMENT</div>
            <div className="flex">
              <img className="h-6 pr-2" src={shopeepay} />
              <img className="h-8" src={dragonpay} />
            </div>
            <div className="py-4">LOGISTICS</div>
            <div>
              <div className="flex">
                <img className="h-4" src={shopeexpress} />
                <img className="h-4 mx-2" src={gogo} />
                <img className="h-4" src={entrego} />
              </div>
              <div className="flex mt-2">
                <img className="h-6" src={ninja} />
                <img className="h-4 mx-2" src={jnt} />
                <img className="h-8" src={lbc} />
              </div>
              <div className="flex">
                <img className="h-8 md:h-12" src={togo} />
                <img className="h-6 mx-2 md:h-10" src={xde} />
                <img className="h-4 md:h-8" src={zoom} />
              </div>
              <img className="h-12" src={mlhuillier} />
            </div>
          </div>
          <div className="w-1/5 ml-8 text-xs md:text-sm">
            <div className="font-semibold">FOLLOW US</div>
            <div>
              <i class="fa fa-facebook" /> Facebook
            </div>
            <div>
              <i class="fa fa-instagram pr-1" />
              Instagram
            </div>
            <div>
              <i class="fa fa-twitter pr-1" />
              Twitter
            </div>
            <div>
              <i class="fa fa-linkedin pr-1" />
              LinkedIn
            </div>
          </div>
          <div className="w-1/5 h-64">
            <div className="text-sm font-semibold">SHOPEE APP DOWNLOAD</div>
            <div>
              <img src={app} />
            </div>
          </div>
        </div>
        <div className="w-full h-16 border-b-2 border-gray-2"></div>
        <div>
          <div className="flex justify-between text-xs md:text-sm">
            <div>© 2020 Shopee. All Rights Reserved</div>

            <div className="flex text-xs md:text-sm">
              <div className="p-2 mr-2 border-r-2">Country&Region:</div>
              <div className="p-2 mr-2 border-r-2">Singapore</div>
              <div className="p-2 mr-2 border-r-2">Indonesia</div>
              <div className="p-2 mr-2 border-r-2">Taiwan</div>
              <div className="p-2 mr-2 border-r-2">Thailand</div>
              <div className="p-2 mr-2 border-r-2">Malaysia</div>
              <div className="p-2 mr-2 border-r-2">Vietnam</div>
              <div className="p-2 mr-2 border-r-2">Philippines</div>
              <div className="p-2 mr-2 ">Brazil</div>
            </div>
          </div>
        </div>
      </div>
      {/* end of Footer */}
    </div>
  );
}

export default Footer;
