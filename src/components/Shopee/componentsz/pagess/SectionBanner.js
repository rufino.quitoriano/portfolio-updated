import React from "react";
import makeCarousel from "react-reveal/makeCarousel";
import Slide from "react-reveal/Slide";
import styled, { css } from "styled-components";
import shopee1 from "../../../../images/shopee1.png";
import shopee2 from "../../../../images/shopee2.jpg";
import shopee3 from "../../../../images/shopee3.jpg";
import shopee4 from "../../../../images/shopee4.jpg";
import shopee5 from "../../../../images/shopee5.jpg";
import shopee6 from "../../../../images/shopee6.jpg";
import shopee7 from "../../../../images/shopee7.png";
import shopee8 from "../../../../images/shopee8.jpg";

const width = "100%",
  height = "250px";
const Container = styled.div`
  position: static;
  overflow: hidden;
  width: ${width};
`;
const Children = styled.div`
  width: ${width};
  position: relative;
  height: ${height};
`;
const Arrow = styled.div`
  text-shadow: 1px 1px 1px #fff;
  z-index: 100;
  line-height: ${height};
  text-align: center;
  position: absolute;
  top: 0;
  width: 10%;
  font-size: 3em;
  cursor: pointer;
  user-select: none;
  ${(props) =>
    props.right
      ? css`
          left: 90%;
        `
      : css`
          left: 0%;
        `}
`;
const Dot = styled.span`
  font-size: 2em;
  cursor: pointer;
  text-shadow: 1px 1px 1px #008b8b;
  user-select: none;
`;
const Dots = styled.span`
  text-align: center;
  width: ${width};
  z-index: 100;
`;
const CarouselUI = ({ position, total, handleClick, children }) => (
  <Container>
    <Children>
      {children}
      <Arrow onClick={handleClick} data-position={position - 1}>
        {"<"}
      </Arrow>
      <Arrow right onClick={handleClick} data-position={position + 1}>
        {">"}
      </Arrow>
    </Children>
  </Container>
);
const Carousel = makeCarousel(CarouselUI);

function SectionBanner() {
  return (
    <div>
      <div className="w-full px-12 md:px-32">
        <div className="w-full h-4"></div>
        <div className="flex">
          <Carousel>
            <Slide right>
              <div className="flex justify-center h-72">
                <img
                  className="w-full h-full"
                  src="https://cf.shopee.ph/file/ed6cbd4221e6be06ec34f82be07ee2c2_xxhdpi"
                />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee1} />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee2} />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee3} />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee4} />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee5} />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee6} />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee7} />
              </div>
            </Slide>
            <Slide right>
              <div className="flex justify-center h-72">
                <img className="w-full h-full" src={shopee8} />
              </div>
            </Slide>
          </Carousel>
          <div className="flex items-center justify-center h-full ml-2">
            <div className="flex flex-col items-center justify-center">
              <div className="mb-2">
                <img
                  className="h-32"
                  src="https://cf.shopee.ph/file/56245574c317d47d3dce64986e9fa941_xhdpi"
                />
              </div>

              <div>
                <img
                  className="h-32"
                  src="https://cf.shopee.ph/file/e64eaaf3060f5735cde45023705ed60c_xhdpi"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SectionBanner;
