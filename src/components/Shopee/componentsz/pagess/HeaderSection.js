import React from "react";

function HeaderSection() {
  return (
    <div className="w-full px-12 md:px-40">
      <div className="flex items-center justify-between h-16 mt-8 text-gray-900 md:mt-2 md:mb-12">
        <div className="flex">
          <div className="px-2 border-r-2">SHOPEE MALL</div>
          <div className="px-2">100% Authentic</div>
          <div className="px-2">7-Day Returns</div>
          <div className="px-2">Free Shipping & COD</div>
        </div>
        <button>
          <div className="px-2">See All</div>
        </button>
      </div>
      <div className="flex mt-2 h-96">
        <div className="flex items-center justify-center w-1/3">
          <button>
            <img src="https://cf.shopee.ph/file/56d95b2bd841c483a6dfbe1a30d668fc" />
          </button>
        </div>
        <div className="flex flex-col justify-between w-full">
          <div className="flex justify-between">
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/d6744ff826aabd364e4932740b20cc82_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/9434b9679d3aa83a32978ce48a57bbc0_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/b9f0b15fc362c4bb752aaf82a863f314_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/16967ef5ff11f44be21ede49ad1807b5_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
          </div>
          <div className="flex justify-between">
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/63af5e2f06e628d67f53a819510ff696_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/99facd7be5c8c3025de95c442f52c2c9_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/51ff2063db3ecf6adff225757a3dcb80_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
            <div className="flex flex-col justify-center w-1/4 m-1">
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/b608d7525c54d8965c55a3c5ccc24efa_xhdpi"
              />
              <div className="text-xl font-semibold text-center text-red-500">
                SHOP NOW
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="flex items-center justify-between h-16 mt-28 md:mt-16">
          <div className="m-2">TRENDING SEARCHES</div>
          <button>
            <div className="flex m-2 text-red-300">
              <div className="w-4 h-4 m-2">
                <svg viewBox="0 0 12 15" class="shopee-svg-icon icon-refresh">
                  <path
                    fill="red"
                    d="M12 7.51268255c0-1.71021918-.7226562-3.30538371-1.9648437-4.43447938-.20507817-.18525749-.52148442-.16965686-.7070313.03315134-.18554687.20475828-.16992188.52067106.03320313.70592856C10.3984375 4.75722109 11 6.08717488 11 7.51268255c0 2.59360495-1.98242187 4.72699125-4.515625 4.96880095l.68164063-.7878318c.1796875-.2086585.15625-.5245713-.05273438-.7039785-.20898438-.1794073-.52539062-.1560063-.70507813.0526521l-1.49609375 1.7336201c-.18164062.2106086-.15625.5284714.05664063.7078787l1.65429688 1.3982065c.21093749.1774572.52539062.1521062.70507812-.0585023.17773438-.2106085.15234375-.5245712-.05859375-.7039785l-.75195313-.6357257C9.58789062 13.2205634 12 10.6484094 12 7.51268255zM2.80273438 11.3523879C1.66796875 10.4085497 1 9.0161934 1 7.51463263c0-2.75741154 2.23828125-4.99220194 5-4.99220194h.01367188l-.7734375.75078037c-.19726563.19305781-.203125.50897059-.00976563.70592855.19335938.19695797.50976563.20280821.70703125.0097504l1.64257813-1.59516453c.19921875-.19305781.20117187-.51287074.00585937-.70982871L6.06054688.14723461c-.1953125-.19500789-.51171875-.19695797-.70703125-.00195008C5.15820313.34029242 5.15625.6562052 5.3515625.85121309l.66992188.67472729H6c-3.31445312 0-6 2.68135846-6 5.99064232 0 1.8018729.80273438 3.4750406 2.16210938 4.6060863.21289062.1755071.52734375.148206.70507812-.0643526.17773438-.2164587.1484375-.5304214-.06445312-.7059285z"
                    fill-rule="nonzero"
                  ></path>
                </svg>
              </div>
              <p className="flex items-center">Change</p>
            </div>
          </button>
        </div>
      </div>
      <div className="flex justify-between">
        <div className="flex items-center justify-center w-1/5 m-2">
          <div className="flex flex-col">
            <p className="font-semibold">Oversized Shirt</p>
            <p className="text-sm">41k+ products</p>
          </div>
          <div className="flex items-center w-24 h-32">
            <img src="https://cf.shopee.ph/file/179a44272c4469a207873ad5aaf78460" />
          </div>
        </div>
        <div className="flex items-center justify-center w-1/5 m-2">
          <div className="flex flex-col">
            <p className="font-semibold">Study Table</p>
            <p className="text-sm">15k+ products</p>
          </div>
          <div className="flex items-center w-24 h-32">
            <img src="https://cf.shopee.ph/file/e7201c92acc1f0ab7aabcb2a7e6a4869" />
          </div>
        </div>
        <div className="flex items-center justify-center w-1/5 m-2">
          <div className="flex flex-col">
            <p className="font-semibold">Korean Top</p>
            <p className="text-sm">596k+ products</p>
          </div>
          <div className="flex items-center w-24 h-32">
            <img src="https://cf.shopee.ph/file/87a628b2ad360168b5d4f6e8901745f7" />
          </div>
        </div>
        <div className="flex items-center justify-center w-1/5 m-2">
          <div className="flex flex-col">
            <p className="font-semibold">Surgical Mask</p>
            <p className="text-sm">9k+ products</p>
          </div>
          <div className="flex items-center w-24 h-32">
            <img src="https://cf.shopee.ph/file/53ca94cc980ef7b5e398db250de228ac" />
          </div>
        </div>
        <div className="flex items-center justify-center w-1/5 m-2">
          <div className="flex flex-col">
            <p className="font-semibold">Marikina Sandals</p>
            <p className="text-sm">9k+ products</p>
          </div>
          <div className="flex items-center w-24 h-32">
            <img src="https://cf.shopee.ph/file/c314b4c4a85b8e7abd1b3bcfa52c26c8" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeaderSection;
