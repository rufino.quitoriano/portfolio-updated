import React from "react";

function ContentCategories() {
  return (
    <div className="w-full px-12 md:px-32">
      <div className="w-full h-16 border-b-2 border-gray-2"></div>
      <div className="w-full h-16"></div>

      <p className="pb-2 font-semibold">Categories</p>
      <div className="flex">
        <div className="flex flex-col w-1/5 mr-8">
          <p className="text-sm font-semibold">MEN'S APPAREL</p>
          <p className="pb-2 text-sm">
            Tops | Jacket & Outerwear | Underwear | Shorts | Pants | Socks |
            Hoodies & Sweatshirts | Others
          </p>
          <p className="text-sm font-semibold">MAKEUP & FRAGRANCES</p>
          <p className="pb-2 text-sm">
            Eye Makeup | Lip Makeup | Fragrances | Face Makeup | Palettes &
            Makeup Sets | Tools & Accessories | Nails | Others
          </p>
          <p className="text-sm font-semibold">HOME & LIVING</p>
          <p className="pb-2 text-sm">
            Seasonal Decor | Home Improvement | Party & Event Supplies |
            Furniture | Kitchenware | Dinnerware | Storage & Organization | Home
            Decor | Lighting | Beddings | Bath | Outdoor & Garden | Home
            Maintenance | Others
          </p>
          <p className="text-sm font-semibold">MEN'S BAGS & ACCESSORIES</p>
          <p className="pb-2 text-sm">
            Hats & Caps | Wallets | Eyewear | Accessories | Jewelry | Watches |
            Men's Bags | Others
          </p>
          <p className="text-sm font-semibold">WOMEN'S SHOES</p>
          <p className="text-sm">
            Flats | Heels | Wedges & Platforms | Sneakers | Flip-flops | Shoe
            Care & Accessories | Boots | Others
          </p>
        </div>
        <div className="flex flex-col w-1/5 mr-8">
          <p className="text-sm font-semibold">WOMEN'S APPAREL</p>
          <p className="pb-2 text-sm">
            Dresses | Tops | Tees | Pants | Skirts | Shorts | Jackets &
            Outerwear | Jumpsuits & Rompers | Lingerie & Nightwear | Swimsuits |
            Coordinates | Socks & Stockings | Plus Size | Sweater & Cardigans |
            Others
          </p>
          <p className="text-sm font-semibold">HOME ENTERTAINMENT</p>
          <p className="pb-2 text-sm">
            Speakers and Karaoke | Projectors | TV Accessories | Television |
            Others
          </p>
          <p className="text-sm font-semibold">CAMERAS</p>
          <p className="pb-2 text-sm">
            Car/Dash Camera | Drones | CCTV/ IP Camera | Action Camera | Camera
            Accessories | Digital Camera | Others | Photo Printing
          </p>
          <p className="text-sm font-semibold">WOMEN'S BAGS</p>
          <p className="pb-2 text-sm">
            Shoulder Bags | Tote Bags | Handbags | Clutches | Backpacks |
            Drawstring Bags | Accessories | Others
          </p>
          <p className="text-sm font-semibold">DIGITAL GOODS & VOUCHERS</p>
          <p className="pb-2 text-sm">
            Travel | Dining | Leisure & Entertainment | Services | Digital
            Currencies & Subscription Vouchers | Others | Shopee Official |
            Telco
          </p>
        </div>
        <div className="flex flex-col w-1/5 mr-8">
          <p className="text-sm font-semibold">MOBILES & GADGETS</p>
          <p className="pb-2 text-sm">
            Portable Audio | Wearables | E-cigarettes | Tablets | Mobiles |
            Others
          </p>
          <p className="text-sm font-semibold">HOME APPLIANCES</p>
          <p className="pb-2 text-sm">
            Small Kitchen Appliances | Cooling & Heating | Vacuum Cleaners &
            Floor Care | Garment Care | Large Appliances | Home Appliance Parts
            & Accessories | Humidifiers & Air Purifiers | Others
          </p>
          <p className="text-sm font-semibold">GROCERIES</p>
          <p className="pb-2 text-sm">
            Beverages | Seasoning, Staple Foods & Baking Ingredients | Alcoholic
            Beverages | Snacks and Sweets | Laundry & Household Product |
            Superfoods & Healthy Foods | Breakfast Food | Others | Frozen &
            Fresh foods
          </p>
          <p className="text-sm font-semibold">MEN'S SHOES</p>
          <p className="pb-2 text-sm">
            Loafer & Boat Shoes | Sneakers | Sandals & Flip-flops | Boots |
            Formal | Shoe Care & Accessories | Others
          </p>
          <p className="text-sm font-semibold">HOBBIES & STATIONERY</p>
          <p className="pb-2 text-sm">
            Musical Instruments | Books & Magazines | Packaging & Wrapping |
            School & Office Supplies | Arts & Crafts | Religious Artifacts |
            Paper Supplies | Writing Materials | Others
          </p>
        </div>
        <div className="flex flex-col w-1/5 mr-8">
          <p className="text-sm font-semibold">HEALTH & PERSONAL CARE</p>
          <p className="pb-2 text-sm">
            Skin Care | Bath & Body | Whitening | Health Supplements | Men's
            Grooming | Personal Care | Hair | Medical Supplies | Slimming |
            Suncare | Sexual Wellness | Others
          </p>
          <p className="text-sm font-semibold">BABIES & KIDS</p>
          <p className="pb-2 text-sm">
            Diapers & Wipes | Babies' Fashion | Girls' Fashion | Boys' Fashion |
            Feeding & Nursing | Bath & Skin Care | Moms & Maternity | Baby Gear
            | Health & Safety | Nursery | Baby Detergent | Others
          </p>
          <p className="text-sm font-semibold">SPORTS & TRAVEL</p>
          <p className="pb-2 text-sm">
            Exercise & Fitness | Travel Bags | Travel Accessories | Men's
            Activewear | Women's Activewear | Sports Bags | Camping & Hiking |
            Cycling, Skates & Scooters | Team Sports | Racket Sports | Leisure
            Sports & Game Room | Outdoor Recreation | Golf | Water Sports |
            Weather Protection | Winter Sports Gear | Boxing & MMA |
            Cockfighting | Others
          </p>
          <p className="text-sm font-semibold">WOMEN'S ACCESSORIES</p>
          <p className="pb-2 text-sm">
            Watches | Jewelry | Wallets & Pouches | Eyewear | Hair Accessories |
            Hats & Caps | Belts & Scarves | Fine Jewelry | Watch & Jewelry
            Organizers | Others
          </p>
          <p className="text-sm font-semibold">PET CARE</p>
          <p className="pb-2 text-sm">
            Pet Grooming Supplies | Pet Toys & Accessories | Pet Essentials |
            Pet Food & Treats | Others
          </p>
        </div>
        <div className="flex flex-col w-1/5 mr-8">
          <p className="text-sm font-semibold">MOBILE ACCESSORIES</p>
          <p className="pb-2 text-sm">
            Powerbanks & Chargers | Cases & Covers | Others | Attachments
          </p>
          <p className="text-sm font-semibold">LAPTOPS & COMPUTERS</p>
          <p className="pb-2 text-sm">
            USB Gadgets | Computer Hardware | Software | Printers and Inks |
            Storage | Computer Accessories | Network Components | Laptops and
            Desktops | Others
          </p>
          <p className="text-sm font-semibold">TOYS, GAMES & COLLECTIBLES</p>
          <p className="pb-2 text-sm">
            Boards & Family Games | Electronic Toys | Collectibles | Educational
            Toys | Dolls | Dress Up & Pretend | Sports & Outdoor Toys |
            Celebrity Merchandise | Others
          </p>
          <p className="text-sm font-semibold">MOTORS</p>
          <p className="pb-2 text-sm">
            Automotive Parts | Exterior Car Accessories | Interior Car
            Accessories | Car Electronics | Motorcycle & ATV Parts | Motorcycle
            Accessories | Car Care & Detailing | Moto Riding & Protective Gear |
            Tools & Garage | Oils, Coolants, & Fluids | Car Services | Others
          </p>
          <p className="text-sm font-semibold">GAMING</p>
          <p className="pb-2 text-sm">
            Console Gaming | Computer Gaming | Mobile Gaming | Others
          </p>
        </div>
      </div>
      <div className="w-full h-32"></div>
    </div>
  );
}

export default ContentCategories;
