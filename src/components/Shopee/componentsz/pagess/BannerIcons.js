import React from "react";

function BannerIcons() {
  return (
    <div className="w-full px-12 md:px-40">
      <div className="flex items-center justify-between w-full h-32">
        <button>
          <div className="flex flex-col items-center justify-center w-10 h-10">
            <img src="https://cf.shopee.ph/file/3f7950b73f83d02f9e4e85dacdaff081_xxhdpi" />
            <div>
              <div className="text-sm text-center">Shopee Live</div>
            </div>
          </div>
        </button>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/19a0e1075e50d350051abf39fbfd1da2_xxhdpi" />
          <div className="text-sm text-center">Free Shipping</div>
        </div>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/5f9662e33be6710f0f9c298af239263e_xxhdpi" />
          <div className="text-sm text-center">Coins Rewards</div>
        </div>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/b1591242645077781988fab2c9c173ff_xxhdpi" />
          <div className="text-sm text-center">ShopeePay</div>
        </div>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/32deffd35c5e9aafcb8ea1368205d710_xxhdpi" />
          <div className="text-sm text-center">20% Cashback</div>
        </div>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/cd9bc6084b2862a964b2faf6e2af6886_xxhdpi" />
          <div className="text-sm text-center">Shopee Milyonaryo</div>
        </div>

        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/2cd7fb02ccaf6dbd485b762bcd98e658_xxhdpi" />
          <div className="text-sm text-center">Shopee Mall</div>
        </div>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/a7f1e6f7465f722ab5db4c537517a25d_xxhdpi" />
          <div className="text-sm text-center">Games</div>
        </div>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/6ce994b4b3cd21f0067a2afc94c9d160_xxhdpi" />
          <div className="text-sm text-center">Shopee Farm</div>
        </div>
        <div className="flex flex-col items-center justify-center w-10 h-10">
          <img src="https://cf.shopee.ph/file/3f9c38c4851369e3d6de6d402b6c0c05_xxhdpi" />
          <div className="text-xs text-center">Shopee Mart</div>
        </div>
      </div>

      <div className="flex items-center justify-center h-32 mt-2">
        <img src="https://cf.shopee.ph/file/5aa7e5953bfbfb3e98ff7bda9533a4bd_xxhdpi" />
      </div>
    </div>
  );
}

export default BannerIcons;
