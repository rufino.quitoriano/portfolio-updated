import React from "react";

function TopProducts() {
  return (
    <div className="w-full px-12 md:px-32">
      <div>
        <div className="flex items-center justify-between h-16 mt-4 mb-2">
          <div className="m-2">TOP PRODUCTS</div>
          <div className="m-2 text-red-300">See all</div>
        </div>
      </div>
      <div className="flex justify-between">
        <div className="flex">
          <div>
            <img
              className="h-96"
              src="https://cf.shopee.ph/file/9564bef76d2b41a06d86dc1da840624f_tn"
            />
            <div className="pt-4 text-sm font-semibold md:text-xl">
              Mumu Bags
            </div>
          </div>
          <div>
            <div>
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/ccddd8904ea457dbb52550092ecb2138_tn"
              />
              <div>
                <img
                  className="h-48"
                  src="https://cf.shopee.ph/file/93607c40bbfc05a65a3918d4f6672d8c_tn"
                />
              </div>
            </div>
          </div>
        </div>

        <div className="flex mx-4">
          <div>
            <img
              className="h-96"
              src="https://cf.shopee.ph/file/66531bea158e863b61b885f2e8cf2a6a_tn"
            />
            <div className="pt-4 pl-4 text-sm font-semibold md:text-xl">
              Medical Mask
            </div>
          </div>
          <div>
            <div>
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/d28e3099d41ad1dca5efc7b7e9733f89_tn"
              />
              <div>
                <img
                  className="h-48"
                  src="https://cf.shopee.ph/file/f8ef5dfa91b439aa436f876c28dd5ae4_tn"
                />
              </div>
            </div>
          </div>
        </div>

        <div className="flex">
          <div>
            <img
              className="h-96"
              src="https://cf.shopee.ph/file/d6dc5bcd09f98226ac140529cc0d8434_tn"
            />
            <div className="pt-4 pl-6 text-sm font-semibold md:pl-10 md:text-xl">
              Tassel Curtain
            </div>
          </div>
          <div>
            <div>
              <img
                className="h-48"
                src="https://cf.shopee.ph/file/618a920494a6d2cccdd2cb815aaf6575_tn"
              />
              <div>
                <img
                  className="h-48"
                  src="https://cf.shopee.ph/file/8ee233d83d302c9abfcc9af936225a5b_tn"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TopProducts;
