import React from "react";
import NavBars from "../../Shopee/componentsz/Navbars";
import BannerIcons from "./pagess/BannerIcons";
import Categories from "./pagess/Categories";
import Content from "./pagess/Content";
import ContentCategories from "./pagess/ContentCategories";
import DailyDiscover from "./pagess/DailyDiscover";
import FlashDeals from "./pagess/FlashDeals";
import Footer from "./pagess/Footer";
import HeaderSection from "./pagess/HeaderSection";
import SectionBanner from "./pagess/SectionBanner";
import TopProducts from "./pagess/TopProducts";

function Shopee() {
  return (
    <div className="absolute">
      <div>
        <NavBars />
      </div>
      <div>
        <SectionBanner />
        <BannerIcons />
        <Categories />
        <FlashDeals />
        <HeaderSection />
        <TopProducts />
        <DailyDiscover />
        <Content />
        <ContentCategories />
        <Footer />
      </div>
    </div>
  );
}

export default Shopee;
