import "./styles/main.css";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import "./App.css";
import TodoList from "./components/todos/TodoList";
import Login from "./components/login/Login";
import Shopee from "./components/Shopee/componentsz/Shopee";
import Hackreactor from "./components/hackReactor/Hackreactor";
import Dashboard from "./components/dashboard/Dashboard";
import Twitter from "./components/twitter/Twitter";
import Test from "./components/test/Test";

function App() {
  return (
    <div>
      <Switch>
        <Route path="/shopee" component={Shopee} />
        <Route path="/todo" component={TodoList} />
        <Route path="/login" component={Login} />
        <Route path="/hackreact" component={Hackreactor} />
        <Route path="/twitter" component={Twitter} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/test" component={Test} />
        <Route path="/" component={Home} />
      </Switch>
    </div>
  );
}

export default App;
