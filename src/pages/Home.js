import React from "react";
import AboutMe from "./AboutMe";
import Contacts from "./Contacts";
import Experience from "./Experience";
import Messages from "./Messages";
import RecentWorks from "./RecentWorks";
import Skills from "./Skills";
import Fade from "react-reveal/Fade";
import Flip from "react-reveal/Flip";
import cloud1 from "../images/cloud1.svg";
import cloud2 from "../images/cloud2.svg";
import Navbar from "../components/navbar/Navbar";

function Home() {
  return (
    <div className="bg-cyan-900" id="#">
      <Navbar />
      <div className="px-4 md:px-80">
        <div className="h-20 md:h-52"></div>
        <div className="min-h-screen bg-cyan-900" id="#">
          <div>
            <div className="flex flex-col-reverse items-center justify-between md:flex-row">
              <div className="pt-32 font-serif text-yellow-100 ">
                <div className="ml-4 text-2xl md:ml-0 md:text-9xl">
                  <Fade top>QUITORIANO,</Fade>
                </div>
                <div className="pb-4 ml-4 text-2xl md:ml-0 md:text-9xl">
                  <Fade bottom>RUFINO Jr.</Fade>
                </div>
                <div className="text-base md:text-5xl">
                  <Flip bottom cascade>
                    Web Designer
                  </Flip>
                </div>
                <div className="text-base md:text-5xl">
                  <Flip bottom cascade>
                    Junior Web
                  </Flip>
                </div>
                <div className="text-base md:text-5xl">
                  <Flip bottom cascade>
                    Developer
                  </Flip>
                </div>
              </div>
              <div className="flex justify-center">
                <div className="absolute flex flex-col items-center justify-center">
                  <Fade left>
                    <div>
                      <img src={cloud1} />
                    </div>
                  </Fade>
                  <Fade bottom>
                    <div className="text-6xl font-bold text-red-700 md:text-7xl">
                      PORTFOLIO
                    </div>
                  </Fade>
                  <Fade right>
                    <div>
                      <img src={cloud2} />
                    </div>
                  </Fade>
                </div>
                <div className="w-64 h-64 mt-20 bg-yellow-100 rounded-full md:h-96 md:w-96"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Skills />
      <Experience />
      <RecentWorks />
      <AboutMe />
      <Contacts />
      <Messages />
    </div>
  );
}

export default Home;
