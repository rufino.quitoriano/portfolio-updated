import React from "react";
import {
  FaHtml5,
  FaCss3Alt,
  FaSass,
  FaBootstrap,
  FaReact,
  FaNpm,
  FaGitAlt,
  FaGithubSquare,
} from "react-icons/fa";
import { SiJavascript } from "react-icons/si";
import Roll from "react-reveal/Roll";
import Rotate from "react-reveal/Rotate";
import Flip from "react-reveal/Flip";
function Skills() {
  return (
    <div className="min-h-screen px-4 md:px-32" id="skills">
      <div className="h-48 md:h-28"></div>
      <div className="flex justify-center pb-2 text-yellow-100 border-b-4 md:pb-8">
        <div className="text-4xl font-bold tracking-wider md:text-9xl">
          <Roll bottom cascade>
            Skills
          </Roll>
        </div>
      </div>
      <div>
        <div className="flex justify-center">
          <div className="h-full px-2 md:w-3/5">
            <div className="flex flex-wrap items-center justify-between">
              <Rotate top left>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5  m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <FaHtml5 />
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    HTML 5
                  </div>
                </div>
              </Rotate>
              <Flip bottom>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5  m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <FaCss3Alt />
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    CSS 3
                  </div>
                </div>
              </Flip>
              <Rotate top right>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5  m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <FaSass />
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    SASS
                  </div>
                </div>
              </Rotate>
              <Rotate top left>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5  m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <FaBootstrap />
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    BOOTSTRAP
                  </div>
                </div>
              </Rotate>
              <Flip bottom>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5 m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <FaReact />
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    REACT
                  </div>
                </div>
              </Flip>
              <Rotate top right>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5 m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <FaNpm />
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    NPM
                  </div>
                </div>
              </Rotate>
              <Rotate top left>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5 m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <div className="flex items-center justify-center">
                      <FaGitAlt />
                    </div>
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    GIT ALT
                  </div>
                </div>
              </Rotate>
              <Flip bottom>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5 m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <div className="flex items-center justify-center">
                      <FaGithubSquare />
                    </div>
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    GIT HUB
                  </div>
                </div>
              </Flip>
              <Rotate top right>
                <div className="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ... w-1/5 m-4 md:m-8 text-yellow-100 text-4xl md:text-9xl">
                  <div className="flex items-center justify-center">
                    <div className="flex items-center justify-center">
                      <SiJavascript />
                    </div>
                  </div>
                  <div className="text-sm font-bold text-center text-yellow-100 md:text-2xl">
                    JAVASCRIPT
                  </div>
                </div>
              </Rotate>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Skills;
