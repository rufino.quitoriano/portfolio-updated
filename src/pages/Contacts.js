import React from "react";
import chapel from "../images/chapel.jpg";
import Roll from "react-reveal/Roll";
import Fade from "react-reveal/Fade";
import Bounce from "react-reveal/Bounce";
import Rotate from "react-reveal/Rotate";
function Contacts() {
  return (
    <div className="min-h-screen px-4 md:px-32" id="contacts">
      <div className="h-36 md:h-28"></div>
      <div className="flex justify-center pb-8 text-yellow-100 border-b-4">
        <div className="text-3xl font-bold tracking-wider md:text-9xl">
          <Roll bottom cascade>
            Let's Talk
          </Roll>
        </div>
      </div>

      <div className="flex w-full">
        <div className="flex items-center justify-center w-1/2 h-full pr-2 mt-8 border-r-4">
          <Rotate bottom right>
            <img className=" w-96 rounded-xl" src={chapel} />
          </Rotate>
        </div>
        <div className="w-1/2">
          <div className="flex justify-center mt-8 text-2xl font-bold text-yellow-100 md:mt-12 md:text-6xl">
            <Fade bottom>Get in Touch</Fade>
          </div>
          <Bounce left cascade>
            <div className="w-full m-2 mr-4 border-4 md:m-4 md:mr-8 rounded-xl bg-cyan-700">
              <div className="mt-2 mb-2 font-serif text-xs tracking-widest text-center text-yellow-100 md:mb-8 md:mt-8 md:w-full md:text-4xl">
                Connect with me via
              </div>
              <div className="flex flex-wrap items-center justify-between px-2 mb-2 md:px-16 md:mb-4">
                <div className="">
                  <div className="text-xs text-yellow-100 md:tracking-wider md:text-3xl">
                    Mobile Number:
                  </div>
                  <div className="text-xs text-yellow-100 md:tracking-wider md:text-3xl">
                    Email:
                  </div>
                </div>
                <div className="pt-2">
                  <div className="text-xs text-yellow-100 md:tracking-wider md:text-3xl">
                    +639086824918
                  </div>
                  <div className="text-xs text-yellow-100 md:tracking-wider md:text-3xl">
                    cahozen@gmail.com
                  </div>
                </div>
              </div>
            </div>
          </Bounce>
          <div className="w-full px-2 m-2 mt-2 border-4 md:m-4 md:px-8 md:mt-24 rounded-xl bg-cyan-700">
            <div className="mt-2 mb-2 font-serif text-2xl tracking-widest text-center text-yellow-100 md:mb-8 md:mt-8 sm:w-full sm:text-4xl">
              <div className="flex items-center justify-between ">
                <div className="pl-2 text-xl text-yellow-100 md:text-6xl ">
                  <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                    <a
                      target="_blank"
                      href="https://www.facebook.com/otaku.zenith.1/"
                    >
                      <div className="flex">
                        <div>
                          <i class="fa fa-facebook"></i>
                        </div>
                      </div>
                    </a>
                  </button>
                </div>
                <div className="text-xl text-yellow-100 md:text-6xl ">
                  <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                    <a
                      target="_blank"
                      href="https://www.instagram.com/otakuzenith/"
                    >
                      <i class="fa fa-instagram"></i>
                    </a>
                  </button>
                </div>

                <div className="text-xl text-yellow-100 md:text-6xl ">
                  <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                    <a
                      target="_blank"
                      href="https://twitter.com/yukino33419124"
                    >
                      <i class="fa fa-twitter"></i>
                    </a>
                  </button>
                </div>

                <div className="pr-2 text-xl text-yellow-100 md:text-6xl ">
                  <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 ...">
                    <a
                      target="_blank"
                      href="https://www.linkedin.com/in/zenith-xd-b207641a2/"
                    >
                      <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Contacts;
