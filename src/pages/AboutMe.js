import React from "react";
import Roll from "react-reveal/Roll";
import stairs from "../images/stairs.jpg";
import Slide from "react-reveal/Slide";

function AboutMe() {
  return (
    <div className="min-h-screen px-4 md:px-32" id="aboutme">
      <div className="h-20 md:h-32"></div>
      <div className="flex justify-center">
        <div>
          <div className="flex justify-center h-12 text-center text-yellow-100 border-b-4 md:h-36 ">
            <div className="text-3xl font-bold tracking-wider md:text-9xl">
              <Roll bottom cascade>
                About Me
              </Roll>
            </div>
          </div>
          <div className="flex items-center justify-center w-full mt-8">
            <div className="text-sm text-yellow-100 md:text-4xl">
              Hello im Rufino Quitoriano Jr. and im a former Seaferer.. and a
              Career Shifter from a Seaferer to a Software Developer due to
              Global Pandemic this last year 2020 uptill now. many of OFW's,
              like me are affected in our Job Field..,So i decided to learn and
              study on field of a Programmer so that i can work and earn even if
              im staying on home.. "Affecting on field of job is not the end,
              i've been search and seek for a better opportunity during this
              Global Pandemic."
              <p className="py-4">
                There are no secrets to success. It is the result of
                preparation, Hard work, and learning from failure.
              </p>
            </div>
          </div>
          <div className="flex justify-center mt-2 md:mt-8">
            <div>
              <Slide bottom>
                <img className="shadow-2xl w-96 rounded-xl" src={stairs} />
              </Slide>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutMe;
