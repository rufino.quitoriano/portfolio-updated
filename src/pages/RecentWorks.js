import React from "react";
import Roll from "react-reveal/Roll";
import makeCarousel from "react-reveal/makeCarousel";
import Slide from "react-reveal/Slide";
import styled, { css } from "styled-components";
import { Link } from "react-router-dom";
import hack from "../images/hack.png";
import shopeee from "../images/shopeee.png";
import dashboardd from "../images/dashboardd.png";
import checklist from "../images/Checklist.png";
import twitterlogo from "../images/twitterlogo.png";
import login1 from "../images/login1.png";

const width = "100%",
  height = "400px";
const Container = styled.div`
  position: static;
  overflow: hidden;
  width: ${width};
`;
const Children = styled.div`
  width: ${width};
  position: relative;
  height: ${height};
`;
const Arrow = styled.div`
  text-shadow: 1px 1px 1px #fff;
  z-index: 100;
  line-height: ${height};
  text-align: center;
  position: absolute;
  top: 0;
  width: 10%;
  font-size: 3em;
  cursor: pointer;
  user-select: none;
  ${(props) =>
    props.right
      ? css`
          left: 90%;
        `
      : css`
          left: 0%;
        `}
`;
const Dot = styled.span`
  font-size: 2em;
  cursor: pointer;
  text-shadow: 1px 1px 1px #008b8b;
  user-select: none;
`;
const Dots = styled.span`
  text-align: center;
  width: ${width};
  z-index: 100;
`;
const CarouselUI = ({ position, total, handleClick, children }) => (
  <Container>
    <Children>
      {children}
      <Arrow onClick={handleClick} data-position={position - 1}>
        {"<"}
      </Arrow>
      <Arrow right onClick={handleClick} data-position={position + 1}>
        {">"}
      </Arrow>
    </Children>
    <Dots className="flex ml-12 md:justify-center md:mt-32 md:ml-0">
      {Array(...Array(total)).map((val, index) => (
        <Dot key={index} onClick={handleClick} data-position={index}>
          {index === position ? "● " : "○ "}
        </Dot>
      ))}
    </Dots>
  </Container>
);
const Carousel = makeCarousel(CarouselUI);

function RecentWorks() {
  return (
    <div>
      <div className="min-h-screen px-4 md:px-32" id="recentworks">
        <div className="h-24 md:h-32" />
        <div className="flex justify-center pb-2 text-yellow-100 border-b-4 md:pb-8">
          <div className="text-base font-bold tracking-tight md:tracking-wider md:text-9xl">
            <Roll bottom cascade>
              Recent Works
            </Roll>
          </div>
        </div>
        <div className="flex justify-center mt-8">
          <div className="w-full h-32 md:h-full">
            <Carousel>
              <Slide right>
                <Link to="/todo">
                  <div className="flex flex-col items-center justify-center mt-4 md:ml-12">
                    <div className="text-2xl font-bold tracking-widest text-center text-yellow-100 md:text-6xl">
                      <div>
                        click here
                        <div className="animate-bounce">
                          <i class="fa fa-caret-down" />
                        </div>
                      </div>
                      <img className="h-64 md:h-80" src={checklist} />
                      TODO
                    </div>
                  </div>
                </Link>
              </Slide>
              <Slide right>
                <Link to="/login">
                  <div className="flex justify-center md:ml-12 ">
                    <div className="text-2xl font-bold tracking-widest text-center text-yellow-100 md:text-6xl">
                      <div>
                        click here
                        <div className="animate-bounce">
                          <i class="fa fa-caret-down" />
                        </div>
                      </div>
                      <img src={login1} />
                      LOG-IN PAGE
                    </div>
                  </div>
                </Link>
              </Slide>
              <Slide right>
                <Link to="/shopee">
                  <div className="flex items-center justify-center text-center md:ml-12">
                    <div className="text-2xl text-yellow-100 md:text-6xl">
                      <div>
                        click here
                        <div className="animate-bounce">
                          <i class="fa fa-caret-down" />
                        </div>
                      </div>
                      <img className="h-94" src={shopeee} />
                      SAMPLE SHOPEE
                    </div>
                  </div>
                </Link>
              </Slide>
              <Slide right>
                <Link to="/hackreact">
                  <div className="flex flex-col items-center justify-center h-80">
                    <div className="mt-12 text-2xl text-center text-yellow-100 md:text-6xl">
                      click here
                      <div className="animate-bounce">
                        <i class="fa fa-caret-down" />
                      </div>
                    </div>
                    <img className="h-64 md:h-40" src={hack} />
                    <div className="mt-6 text-2xl text-yellow-100 md:text-6xl">
                      Sample Hackreact
                    </div>
                  </div>
                </Link>
              </Slide>
              <Slide right>
                <Link to="/dashboard">
                  <div className="flex flex-col items-center">
                    <div className="text-2xl text-center text-yellow-100 md:text-6xl">
                      click here
                      <div className="animate-bounce">
                        <i class="fa fa-caret-down" />
                      </div>
                    </div>
                    <img className="h-64" src={dashboardd} />
                    <div className="text-2xl text-yellow-100 md:text-6xl">
                      SAMPLE DASHBOARD
                    </div>
                  </div>
                </Link>
              </Slide>
              <Slide right>
                <Link to="/twitter">
                  <div className="flex flex-col items-center">
                    <div className="text-2xl text-center text-yellow-100 md:text-6xl">
                      click here
                      <div className="animate-bounce">
                        <i class="fa fa-caret-down" />
                      </div>
                    </div>
                    <img className="h-48 md:h-64" src={twitterlogo} />
                    <div className="mt-12 text-2xl text-yellow-100 md:text-6xl">
                      SAMPLE TWITTER
                    </div>
                  </div>
                </Link>
              </Slide>
              <Slide right>
                <Link to="/test">
                  <div className="flex flex-col items-center">
                    <div className="text-2xl text-center text-yellow-100 md:text-6xl">
                      click here
                      <div className="animate-bounce">
                        <i class="fa fa-caret-down" />
                      </div>
                    </div>
                    <div className="mt-12 text-2xl text-yellow-100 md:text-6xl">
                      test
                    </div>
                  </div>
                </Link>
              </Slide>
            </Carousel>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RecentWorks;
