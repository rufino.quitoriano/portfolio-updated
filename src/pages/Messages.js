import React from "react";
import Bounce from "react-reveal/Bounce";
import Flip from "react-reveal/Flip";
import Slide from "react-reveal/Slide";
function Messages() {
  return (
    <div
      className="flex flex-col justify-center min-h-screen bg-cyan-900"
      id="messages"
    >
      <div className="flex justify-center">
        <div className="p-2 border-2 w-80 md:p-2 md:w-2/5">
          <div className="w-full mb-4 font-serif text-xl font-bold tracking-wider text-yellow-100 text-start md:text-6xl">
            <Bounce bottom cascade>
              Let's Chat
            </Bounce>
          </div>
          <div className="md:w-full">
            <div className="text-gray-100">
              <Flip bottom cascade>
                <input
                  placeholder="Your Name*"
                  className="w-full h-10 p-1 placeholder-yellow-100 bg-teal-500 border-2"
                ></input>
              </Flip>
            </div>

            <div className="mt-6 text-gray-100">
              <Flip bottom cascade>
                <input
                  placeholder="Your email*"
                  className="w-full h-10 p-1 placeholder-yellow-100 bg-teal-500 border-2"
                ></input>
              </Flip>
            </div>

            <div className="mt-6 text-gray-100">
              <Flip bottom cascade>
                <input
                  placeholder="Write a Subject*"
                  className="w-full h-10 p-1 placeholder-yellow-100 bg-teal-500 border-2"
                ></input>
              </Flip>
            </div>

            <div className="flex mt-6 text-gray-100">
              <Flip bottom cascade>
                <textarea
                  placeholder="Your Message*"
                  className="w-full h-32 p-1 placeholder-yellow-100 bg-teal-500 border-2"
                ></textarea>
              </Flip>
            </div>
            <Slide bottom>
              <div className="pt-8">
                <button class="text-gray-100 transition border-2 h-10 w-32 duration-500 ease-in-out bg-cyan-700 hover:bg-teal-500 transform hover:-translate-y-1 hover:scale-110 ...">
                  Submit
                </button>
              </div>
            </Slide>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Messages;
