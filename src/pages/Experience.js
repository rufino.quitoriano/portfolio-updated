import React from "react";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import Roll from "react-reveal/Roll";

function Experience() {
  return (
    <div>
      <div className="min-h-screen px-4 md:px-32" id="experience">
        <div className="h-16 md:h-24"></div>
        <div className="flex justify-center pb-2 text-yellow-100 border-b-4 md:pb-8">
          <div className="text-2xl font-bold md:tracking-wider md:text-9xl">
            <Roll bottom cascade>
              Experience
            </Roll>
          </div>
        </div>
        <VerticalTimeline>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: "rgb(47, 220, 201)", color: "#fff" }}
            date="September 15, 2020 - December 27, 2020"
            iconStyle={{ background: "rgb(122, 199, 194)", color: "#fff" }}
            //   icon={<WorkIcon />}
          >
            <div className="text-2xl font-bold vertical-timeline-element-title">
              Altev Global Solutions
            </div>
            <h4 className="vertical-timeline-element-subtitle">
              Makati Philippines
            </h4>
            <p>Student</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            contentStyle={{ background: "rgb(47, 220, 201)", color: "#fff" }}
            className="vertical-timeline-element--work"
            date="January 16, 2016 - November 05, 2019"
            iconStyle={{ background: "rgb(122, 199, 194)", color: "#fff" }}
            //   icon={<WorkIcon />}
          >
            <div className="text-2xl font-bold vertical-timeline-element-title">
              Hartmann Crew Philippines
            </div>
            <h4 className="vertical-timeline-element-subtitle">
              5/F, CRI Center, 665 Quirino Ave, Malate, Manila, 1004 Metro
              Manila
            </h4>
            <p>Steward</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: "rgb(47, 220, 201)", color: "#fff" }}
            date="April 15, 2015 - January 05, 2016"
            iconStyle={{ background: "rgb(122, 199, 194)", color: "#fff" }}
            //   icon={<WorkIcon />}
          >
            <div className="text-2xl font-bold vertical-timeline-element-title">
              Ifortress Solution
            </div>
            <h4 className="vertical-timeline-element-subtitle">
              The Studio Residences 2 1282 Nuestra Señora St. Brgy. Guadalupe
              Nuevo, Makati City
            </h4>
            <p>Associate</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            contentStyle={{ background: "rgb(47, 220, 201)", color: "#fff" }}
            date="March 13, 2014 - January 06, 2015"
            iconStyle={{ background: "rgb(122, 199, 194)", color: "#fff" }}
            //   icon={<SchoolIcon />}
          >
            <div className="text-2xl font-bold vertical-timeline-element-title">
              Kabisera ng Dencio’s
            </div>
            <h4 className="vertical-timeline-element-subtitle">
              7th street, Bldg. 3 Bonifacio High Street, Taguig City
            </h4>
            <p>Server/Waiter/Bartender</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            contentStyle={{ background: "rgb(47, 220, 201)", color: "#fff" }}
            date="October 20, 2012 - March 20, 2013"
            iconStyle={{ background: "rgb(122, 199, 194)", color: "#fff" }}
            //   icon={<SchoolIcon />}
          >
            <div className="text-2xl font-bold vertical-timeline-element-title">
              Fely J's Kitchen
            </div>
            <h4 className="vertical-timeline-element-subtitle">
              2nd Level Greenbelt 5, Ayala Center, Makati City
            </h4>
            <p>Server/Waiter/Bartender</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            contentStyle={{ background: "rgb(47, 220, 201)", color: "#fff" }}
            date="February 22, 2012 - September 31, 2012"
            iconStyle={{ background: "rgb(122, 199, 194)", color: "#fff" }}
            //   icon={<SchoolIcon />}
          >
            <div className="text-2xl font-bold vertical-timeline-element-title">
              Juan's Bistro
            </div>
            <h4 className="vertical-timeline-element-subtitle">
              Ground Floor, 146 South Parking Building Seaside Blvd, Manila Bay
              Reclamation Area SM Mall of Asia Pasay 556-0638
            </h4>
            <p>Server/Waiter</p>
          </VerticalTimelineElement>
        </VerticalTimeline>
      </div>
    </div>
  );
}

export default Experience;
